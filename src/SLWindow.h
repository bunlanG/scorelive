/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

/**     @class Représente la fenêtre principale
  *
  *     @todo Implémenter "Aide" et "A propos de ScoreLive..."
 **/

#ifndef SLWINDOW_H
#define SLWINDOW_H

#include <QtCore/QTime>
#include <QtGui/QCloseEvent>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QInputDialog>
#include <algorithm>
#include <functional>
#include "SLTournament.h"
#include "SLTFile.h"
#include "SLTableWiz.h"
#include "SLMatchWiz.h"
#include "ui_SLMainWindow.h"


class SLWindow : public QMainWindow {
    Q_OBJECT

    public:
        SLWindow(QWidget *parent = 0);
        ~SLWindow();
        void open(QString const &dirFile);

    private slots:
        void aboutQt();
        void newFile();
        void setupFile(QString nom, int optHeight);
        void openFile();
        void saveFile();
        void saveFileAs();
        void newSLTable();
        void setSLTable();
        void newMatchDay();
        void setMatchDay();
        
        void documentWasModified();

    private:
        //Methodes
        QString getXml();
        bool maybeSave();
        /// @brief Reset optimal size of the window which must show ALL SLTournament
        /// @param forceResize : force the resize, even if the wondow have an suficient height before
        void setOptSize(bool forceResize) {
           int optHeight = qMax(_SLTournament->minimumHeight() + 42, 140);
           int currHeight = _SLTournament->height();
           if((optHeight > currHeight) || forceResize) {
               setMinimumHeight(optHeight);
               resize(width(), optHeight);
           }
        }
        /// @reimp
        void closeEvent(QCloseEvent *event);
        //Attributs
        Ui::SLMainWindow ui;
        SLTournament *_SLTournament;
        SLTFile *_SLTFile;
        QString _dirFile;
        QStatusBar *_statusBar;
        QTime _time;

};


#endif // SLWINDOW_H
