/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive.  If not, see <http://www.gnu.org/licenses/>.
*/

/**     @class Gère la lecture / écriture des fichiers .slt
  *
  *     @todo ...
 **/

#ifndef SLTFILE_H
#define SLTFILE_H

#include <QtCore/QTextStream>
#include <QtWidgets/QWidget>
#include <QtXml/QDomElement>
#include <iostream>
#include "SLData.h"

class SLTable;
class Team;
class Versus;
class MatchDay;
class Match;
class SLTournament;

class SLTFile : public QWidget {
    Q_OBJECT

    public:
        SLTFile(QWidget *parent = 0);
        void load(QString const &path, SLTournament *file);
        void write(QString const &path, QString const &xmlString);

    signals:
        void nvChp(QString nom, int optHeight);

        void setFstJr(int clsDft, int JrDft);
        
    private:
        /// @remark Recursive function
        void parseSLTElements(QDomElement const &elementSLT, SLTournament *file, std::vector<int> const &remark = std::vector<int>());
};

#endif
 //SLTFILE_H
