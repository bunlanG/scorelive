/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

#include "MatchDay.h"

MatchDay::MatchDay(QString const &title, SLTable *slTablePtr, QWidget *parent) : QFrame(parent), _wizard(0), _slTablePtr(slTablePtr), _nbr(0), _title(title) {
    _layout = new QVBoxLayout(this);
        _layout->setSpacing(1);
        _layout->setContentsMargins(0, 0, 0, 0);
        _layout->setSizeConstraint(QLayout::SetMinimumSize);
        setLayout(_layout);
}

MatchDay::~MatchDay() {
    for(int i = 0 ; i < _vectMatch.size() ; i++)
        delete _vectMatch[i];
    _vectMatch.clear();

    delete _wizard;
}

void MatchDay::addItem(Match *item) {
    _nbr++;

    _vectMatch.push_back(item);
    _layout->addWidget(item);

    QObject::connect(item, SIGNAL(contentsChanged()), this, SIGNAL(contentsChanged()));
}

void MatchDay::execWizard(int action, std::vector<QString> matchDayTitles) {
    _wizard = new SLMatchWiz(action);
    QObject::connect(_wizard, SIGNAL(finished(int)), this, SLOT(slMatchWizClosed(int)));
    QObject::connect(_wizard, SIGNAL(loadMD(int)), this, SIGNAL(loadMD(int)));
    
    if(action == MATCHS_NEW) {
        _wizard->setNwMatchPage(matchDayTitles);
        hide();
    } else if(action == MATCHS_EDIT) {
        _wizard->setEdPage(_title, getHstsName(), getVstsName());
    }
    
    _wizard->show();
}

void MatchDay::loadMatchDay(MatchDay const *model) {
    if(_wizard != 0)
        _wizard->loadMatchDay(model->getHstsName(), model->getVstsName());
}

void MatchDay::slMatchWizClosed(int dialogCode) {
   int action = _wizard->getAction();

   if(dialogCode == QDialog::Accepted) {
        if(action == MATCHS_NEW) {
            setupThis();
        } else if(action == MATCHS_EDIT) {
            editThis();
        }

        emit contentsChanged();
    } else if (action == MATCHS_NEW) {
       //Journee non initialisee : suppression par le widget parent
       deleteLater();
    }
    
    delete _wizard;
    _wizard = 0;
}

QString MatchDay::getXml(int const clsLink, int const idLiveGr) const {
    QString text = "";

    text += QString("\t\t\t<MatchDay id=\"%1_%2\" title=\"%3\" >\n")
            .arg(clsLink).arg(idLiveGr).arg(_title);
    for (int i = 0 ; i < _nbr ; i++) {
        text += _vectMatch[i]->getXml(); //Match
    }
    text += "\t\t\t</MatchDay>\n";

    return text;
}

QString MatchDay::title() const {
   return _title;
}

std::vector<QString> MatchDay::getHstsName() const {
    std::vector<QString> tab;
    
    for(int i = 0 ; i < _vectMatch.size() ; i++) {
        tab.push_back(_vectMatch[i]->getHstName());
    }

    return tab;
}

std::vector<QString> MatchDay::getVstsName() const {
    std::vector<QString> tab;
    
    for(int i = 0 ; i < _vectMatch.size() ; i++) {
        tab.push_back(_vectMatch[i]->getVstName());
    }

    return tab;
}

void MatchDay::setupThis() {
    int nbrMatchs = _wizard->getNbrMt();
    std::vector< std::vector<QString> > teamsNames = _wizard->getTeamsNames();
    std::vector<bool> noDraws = _wizard->getNoDraws();
    
    std::vector< std::vector<int> > score;
    std::vector<int> scoreDef(2,0);
    
    for(int i = 0 ; i < nbrMatchs ; i++) {
        //score
        score.push_back(scoreDef);
        if(noDraws[i]) {
            score.push_back(scoreDef);
            score.push_back(scoreDef);
        }
        
        addItem(new Match(teamsNames[i], score, 2, noDraws[i], false, _slTablePtr));
        
        score.clear();
    }

    show();
}

void MatchDay::editThis() {
    std::vector<int> nwOrder = _wizard->getOrdre();
    
    //title
    _title = _wizard->getMatchDayName();

    //le reste :
    //on edite le nouvel ordre
    for(int i = 0 ; i < nwOrder.size() ; i++) {
        _vectMatch[i]->setnvOrdre(nwOrder[i]);
    }

    //on enleve TOUS les objets Match du layout
    for(int i = 0 ; i < _vectMatch.size() ; i++) {
        _layout->removeWidget(_vectMatch[i]);
    }

    //on trie
    sort(_vectMatch.begin(), _vectMatch.end(), sortMatch());

    //on remet les objets Match : ils seront dans l'ordre !
    for(int i = 0 ; i < _vectMatch.size() ; i++) {
        _layout->addWidget(_vectMatch[i]);
    }
}
