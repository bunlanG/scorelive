/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

/**     @class Module gérant tous les MatchDay
  *
  *     @todo ...
 **/

#ifndef RESULTS_H
#define RESULTS_H

#include <QtWidgets/QMessageBox>
#include "MatchDay.h"
#include "ui_ResultsHead.h"


class Results : public QFrame {
    Q_OBJECT

    public:
        Results(QWidget *parent = 0);
        ~Results();
        void clear();
        void ajGroupe();
        void delLastSlTable();
        void setFstJr(int const clsDft, int const jrDft);
        void addItem(MatchDay *item, int const grLink);
        void addItem(Match *item, int const grLink, int const journee);
        QString getXml() const;

        //Edition of the shown MatchDay
        void execMatchDayWizard(int action, SLTable *slTablePtr);

    signals:
        void changeSLTable(int nwSLTableId);
        void contentsChanged();
        
    private slots:
        void upBut();
        void downBut();
        void rightBut();
        void leftBut();
        
        void loadMatchDay(int index);
        void destroyMatchDay(QObject *obj);

    private :
        //Methods
        void majAffNavig();
        void changeMatchDay(int const nvclsLinkActive, int const nvLiveGrActive);
        std::vector<QString> getCurrentSLTblMDTitles(int const SLTableEna) const;
        //Attributes
        Ui::ResultsHead ui;
        QFrame *_head;
        QVBoxLayout *layout;
        std::vector< std::vector<MatchDay*> > tabl;
        QSpacerItem *space;
        int _ClsDft; //Pour l'affichage de la premiere journée lors du chargement d'un fichier
        int _JrDft; //Indique le SLTable associé au MatchDay affiché
        int _JrEna; //Indique la position du MatchDay pointé dans le tableau (ie affiché)
        int _ClsEna; //Indique le SLTable associé au MatchDay affiché
        std::vector<int> _nbr; //Nombre de MatchDay pour chaque groupe
        bool showAnotherMD;   //Always true, except when clear() is called : say if we need to re-show another MatchDay when one is destroyed
};


#endif // RESULTS_H
