/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

/**     @class Représente un versus (COUPE)
  *
  *     @todo ...
 **/

#ifndef VERSUS_H
#define VERSUS_H

#include <QtWidgets/QLabel>
#include <map>
#include "SLExpressions.h"

class Versus : public QLabel {
    
    public:
        Versus(std::vector<QString> const &teamsName, std::vector<int> const &diffs, QWidget *parent = 0);
        
        QString getEq1() const {return _teamsName[0];}
        QString getEq2() const {return _teamsName[1];}
 
        QString getXml() const;
        QString qualifie(bool const rglAway) const;
        
        void update(bool const team1IsHost = false, std::map<QString,int> changeMapTeam1 = std::map<QString,int>(), std::map<QString,int> changeMapTeam2 = std::map<QString,int>());

    private:
        std::vector<int> _diffs;
        std::vector<QString> _teamsName;
};

#endif // VERSUS_H
