/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SLWindow.h"

SLWindow::SLWindow(QWidget *parent) : QMainWindow(parent), _dirFile(""), _time() {
   ui.setupUi(this);
 
   setWindowModified(false);

   _time.start();
   _SLTournament = new SLTournament("", 0);
   _SLTournament->hide();
      QObject::connect(_SLTournament, SIGNAL(contentsChanged()), this, SLOT(documentWasModified()));
      QObject::connect(_SLTournament, SIGNAL(newMatchDay()), this, SLOT(newMatchDay()));
      setCentralWidget(_SLTournament);
   _statusBar = statusBar();
      QLabel *ready = new QLabel(tr("Prêt"), _statusBar);
      _statusBar->addWidget(ready);

    QObject::connect(ui.aboutQt, SIGNAL(triggered()), this, SLOT(aboutQt()));
    QObject::connect(ui.quit, SIGNAL(triggered()), this, SLOT(close()));
    QObject::connect(ui.newFile, SIGNAL(triggered()), this, SLOT(newFile()));
    QObject::connect(ui.newSLTable, SIGNAL(triggered()), this, SLOT(newSLTable()));
    QObject::connect(ui.setSLTable, SIGNAL(triggered()), this, SLOT(setSLTable()));
    QObject::connect(ui.newMatchDay, SIGNAL(triggered()), this, SLOT(newMatchDay()));
    QObject::connect(ui.setMatchDay, SIGNAL(triggered()), this, SLOT(setMatchDay()));
    QObject::connect(ui.openFile, SIGNAL(triggered()), this, SLOT(openFile()));
    QObject::connect(ui.saveFile, SIGNAL(triggered()), this, SLOT(saveFile()));
    QObject::connect(ui.saveFileAs, SIGNAL(triggered()), this, SLOT(saveFileAs()));

    setWindowTitle("ScoreLive");

    _SLTFile = new SLTFile();
        QObject::connect(_SLTFile, SIGNAL(nvChp(QString,int)), this, SLOT(setupFile(QString,int)));

    setOptSize(true);
}

SLWindow::~SLWindow() {
    delete _SLTFile;
    delete _SLTournament;
}

/// @brief open a file with external argumants, e.g. argc/argv
void SLWindow::open(QString const &dirFile) {
    _dirFile = dirFile;

    _time.restart();
    _statusBar->showMessage(tr("Lecture fichier en cours... : (%1)")
                                .arg(_dirFile));

    _SLTFile->load(_dirFile, _SLTournament);
    setWindowModified(false);

    int ms = _time.restart();
    _statusBar->showMessage(tr("(%1 ms) Fichier chargé de %2")
                             .arg(ms).arg(_dirFile), 5000);

    ui.matchDayMenu->setEnabled(true);

    setOptSize(true);
}

void SLWindow::closeEvent(QCloseEvent *event) {
    if(maybeSave())
        event->accept();
    else
        event->ignore();
}

bool SLWindow::maybeSave() {
    if(isWindowModified()) {
        if(_SLTournament != 0) {
            if(!(_SLTournament->isModified())) {
                return true;
            } else {
                int reponse = QMessageBox::information(this, tr("Enregistrer fichier"), tr("Voulez-vous enregistrer le fichier ?"), QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel, QMessageBox::Cancel);

                switch(reponse) {
                    case QMessageBox::Yes:
                        saveFile();
                        return true;
                        break;
                    case QMessageBox::No:
                        return true;
                        break;
                    default:
                        return false;
                        break;
                }
            }
        } else {
            return true;
        }
    } else {
        return true;
    }
}

void SLWindow::aboutQt() {
    QApplication::aboutQt();
}


void SLWindow::newFile() {   
    bool ok = false;
    QString name = QInputDialog::getText(this, tr("Nouveau Championnat"), tr("Nom du Championnat :"), QLineEdit::Normal, QString(), &ok);

    if (ok && !name.isEmpty() && maybeSave()) {
        _dirFile = "";

        setupFile(name, 134);

        ui.matchDayMenu->setEnabled(false);
        newSLTable();

        setOptSize(true);
    }
}

void SLWindow::setupFile(QString nom, int optHeight) {
    _time.restart();
    _statusBar->showMessage(tr("Création fichier en cours... : %1")
                                .arg(nom));

    _SLTournament->clear();
    _SLTournament->setName(nom);

    setWindowTitle("ScoreLive - "+nom+"[*]");
    setWindowModified(false);

    int ms = _time.restart();
    _statusBar->showMessage(tr("(%1 ms) Fichier créé")
                            .arg(ms), 5000);

    ui.saveFile->setEnabled(true);
    ui.saveFileAs->setEnabled(true);
    ui.slTableMenu->setEnabled(true);
    ui.matchDayMenu->setEnabled(true);

   setOptSize(false);
}

void SLWindow::openFile() {
    _dirFile = QFileDialog::getOpenFileName(this, tr("Ouvrir un fichier"), _dirFile, tr("ScoreLiveCompetition/Tournament Files (*.slc *.slt)"));

    if(!_dirFile.isNull() && maybeSave()) {
        _time.restart();
        _statusBar->showMessage(tr("Lecture fichier en cours... : (%1)")
                                .arg(_dirFile));

        _SLTFile->load(_dirFile, _SLTournament);
        setWindowModified(false);

        int ms = _time.restart();
        _statusBar->showMessage(tr("(%1 ms) Fichier chargé de %2")
                                .arg(ms).arg(_dirFile), 5000);

        ui.matchDayMenu->setEnabled(true);

        setOptSize(true);
    }
}

void SLWindow::saveFile() {
    if (_dirFile.isNull()) {
        saveFileAs();
    } else {
        _time.restart();
        _statusBar->showMessage(tr("Enregistrement en cours... (%1)")
                                .arg(_dirFile));

        _SLTFile->write(_dirFile, getXml());
        setWindowModified(false);

        int ms = _time.restart();
        _statusBar->showMessage(tr("(%1 ms) Fichier enregistré dans %2")
                                .arg(ms).arg(_dirFile), 5000);
    }
}

void SLWindow::saveFileAs() {
    _dirFile = QFileDialog::getSaveFileName(this, tr("Enregistrer sous"), _dirFile, tr("ScoreLiveTournament Files (*.slt)"));

    if(!_dirFile.isNull()) {
        _time.restart();
        _statusBar->showMessage(tr("Enregistrement en cours... (%1)")
                                .arg(_dirFile));

        _SLTFile->write(_dirFile, getXml());
        setWindowModified(false);

        int ms = _time.restart();
        _statusBar->showMessage(tr("(%1 ms) Fichier enregistré dans %2")
                                .arg(ms).arg(_dirFile), 5000);
    }
}

void SLWindow::newSLTable() {
    _SLTournament->execSLTableWizard(GROUPE_NEW);
}

void SLWindow::setSLTable() {
    _SLTournament->execSLTableWizard(GROUPE_EDIT);
}

void SLWindow::newMatchDay() {
    _SLTournament->execMatchDayWizard(MATCHS_NEW);
}

void SLWindow::setMatchDay() {
    _SLTournament->execMatchDayWizard(MATCHS_EDIT);
}

void SLWindow::documentWasModified() {
    setWindowModified(true);

    setOptSize(false);
}

QString SLWindow::getXml() {
    QString text = "";

    text += "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
            "<!DOCTYPE SLT>\n\n";
    text += _SLTournament->getXml(); //Infos + Content

    return text;
}
