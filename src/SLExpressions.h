/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

/**     @class fonctions utilisés par les divers modules pour les infos en XML
  *
  *     @todo ...
 **/

#ifndef SLEXPRESSIONS_H
#define SLEXPRESSIONS_H

#include <QtCore/QObject>
#include <vector>

class SLExpressions : public QObject {

    public:
        /// parse expressions
        static std::vector< std::vector<int> > expToIntMatr(QString const &exp);
        static std::vector<int> expToIntVect(QString const &exp);
        static std::vector<QString> expToStrVect(QString const &exp);
        /// parse vectors
        static QString intMatrToExp(std::vector< std::vector<int> > const &vect, QString const &attribute = "");
        static QString intVectToExp(std::vector<int> const &vect, QString const &attribute = "");
        static QString strVectToExp(std::vector<QString> const &vect, QString const &attribute = "");
};

#endif //SLEXPRESSIONS_H
