/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

/**     @class Assistant qui ajoute de nouveaux objets SLTable
  *
  *     @todo ...
 **/

#ifndef SLTABLEWIZ_H
#define SLTABLEWIZ_H

#include <QtWidgets/QWizard>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include "ui_SLTableWizColorItem.h"
#include "ui_SLTableWizCriterionItem.h"
#include "ui_SLTableWizTeamEditHead.h"
#include "ui_SLTableWizTeamEditItem.h"
#include "ui_SLTableWizTeamHead.h"
#include "ui_SLTableWizTeamItem.h"
#include "ui_SLTableWizVersusHead.h"
#include "ui_SLTableWizVersusItem.h"

class SLTable;

enum AssGroupeAction {
    GROUPE_NEW, GROUPE_EDIT
};

/// @enum identifiants des pages [\see \reimp de nextId()]
enum {
    Page_IntroGr, Page_Type, Page_InfosPoule, Page_InfosCoupe, Page_Equipe, Page_Color, Page_Versus,
    Page_EditGr
};


class SLTableWiz : public QWizard {
    Q_OBJECT

    public:
        SLTableWiz(int act, QWidget *parent = 0);
        ~SLTableWiz();
        void setEdPage(int type, QString title, std::vector<QString> names, std::vector<int> mans, std::vector< std::vector<int> > bckGrdColor);

        //Gettors
        int getNbrItems() const {return _nbr;}
        int getAction() const;
        int getType() const;
        QString getTitle() const;
        std::vector< std::vector<int> > getBckGrdColor() const;
        std::vector<int> getCriterions() const;
        std::vector<QString> getTeamsName() const;
        std::vector< std::vector<QString> > getTeamsNames() const;
        bool getRulAwayGls() const {return _rulAwayGls->isChecked();}
        std::vector<int> getMans() const;

    private slots:
        void setNbrItem(int nwNbrItem);

    private:
        //Méthods
        QWizardPage *nwIntroPage();
        QWizardPage *nwTypePage();
        QWizardPage *nwInfosRndRobPage();
        QWizardPage *nwInfosKnkOutPage();
        QWizardPage *nwTeamsPage();
        QWizardPage *nwColorPage();
        QWizardPage *nwVersusPage();
        QWizardPage *edSLTablePage();
        
        int nextId() const;
        
        //Attributes
        Ui::SLTableWizTeamHead uiTeamHeader;
        Ui::SLTableWizVersusHead uiVersusHeader;
        int _nbr;
        int _act;
        QLineEdit *_title;
        QRadioButton *_isRndRob;
        QRadioButton *_isKnkOut;
            QCheckBox *_rulAwayGls;
        QVBoxLayout *_lytTeam;
        QVBoxLayout *_lytVersus;
        QVBoxLayout *_lytEdition;
        QGroupBox *_type;
        std::vector<Ui::SLTableWizTeamItem> uiTeamBody;
        std::vector<Ui::SLTableWizVersusItem> uiVersusBody;
        std::vector<Ui::SLTableWizColorItem> uiColorBody;
        std::vector<Ui::SLTableWizCriterionItem> uiCriterionBody;
        std::vector<Ui::SLTableWizTeamEditItem> uiEditionBody;
        std::vector<QFrame*> _SLTableItems;
};

#endif // SLTABLEWIZ_H
