/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SLExpressions.h"

std::vector< std::vector<int> > SLExpressions::expToIntMatr(QString const &exp) {
    QString number = "";
    std::vector< std::vector<int> > matr;
    std::vector<int> vect;

    for(int i = 0 ; i < exp.size() ; i++) {
        if (exp[i] == '_' || exp[i] == '|') {
            if(number == "")
                number = "0";

            vect.push_back(number.toInt());
            number = "";
            if (exp[i] == '|') {
                matr.push_back(vect);
                vect.clear();
            }
        } else {
            if (exp[i] == '\\') {
                i++;
            }
            number += exp[i];
        }
    }

    if(number != "")
        vect.push_back(number.toInt());
    
    matr.push_back(vect);
    return matr;
}

std::vector<int> SLExpressions::expToIntVect(QString const &exp) {
    QString number = "";
    std::vector<int> vect;

    for(int i = 0 ; i < exp.size() ; i++) {
        if (exp[i] == '_') {
            if(number == "")
                number = "0";

            vect.push_back(number.toInt());
            number = "";
        } else {
            if (exp[i] == '\\') {
                i++;
            }
            number += exp[i];
        }
    }

    if(number != "")
        vect.push_back(number.toInt());

    return vect;
}

std::vector<QString> SLExpressions::expToStrVect(QString const &exp) {
    QString str = "";
    std::vector<QString> vect;

    for(int i = 0 ; i < exp.size() ; i++) {
        if (exp[i] == '_') {

            vect.push_back(str);
            str = "";
        } else {
            if (exp[i] == '\\') {
                i++;
            }
            str += exp[i];
        }
    }

    if(str != "")
        vect.push_back(str);

    return vect;
}

QString SLExpressions::intMatrToExp(std::vector< std::vector<int> > const &vect, QString const &attribute) {
    QString exp = "";
    int vectSize = vect.size();

    if(vectSize > 0) {
        if(attribute != "")
            exp += attribute+"=\"";
        for(int i = 0 ; i < vectSize ; i++) {
            for(int j = 0 ; j < vect[i].size() ; j++) {
                if(j > 0)
                    exp += "_";
                exp += QString::number(vect[i][j]);
            }
            if(i < vectSize - 1)
                exp += "|";
        }
    }
    
    if(attribute != "") 
        exp += "\" ";

    return exp;
}

QString SLExpressions::intVectToExp(std::vector<int> const &vect, QString const &attribute) {
    QString exp = "";

    if(vect.size() > 0) { 
        if(attribute != "") 
            exp += attribute+"=\"";
        for(int i = 0 ; i < vect.size() ; i++) {
            exp += QString::number(vect[i]);
            if (i != vect.size() - 1)
                exp += "_";
        }
        
        
        if(attribute != "") 
            exp += "\" ";
    }

    return exp;
}

QString SLExpressions::strVectToExp(std::vector<QString> const &vect, QString const &attribute) {
    QString exp = "";

    if(vect.size() > 0) { 
        if(attribute != "") 
            exp += attribute+"=\"";
        for(int i = 0 ; i < vect.size() ; i++) {
            exp += vect[i];
            if (i != vect.size() - 1)
                exp += "_";
        }
    }
    
 
    if(attribute != "") 
        exp += "\" ";

    return exp;
}
