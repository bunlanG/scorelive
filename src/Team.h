/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive.  If not, see <http://www.gnu.org/licenses/>.
*/

/**     @class Représente une équipe dans un classement (POULE)
  *
  *     @todo ...
 **/

#ifndef TEAM_H
#define TEAM_H

#include "SLExpressions.h"
#include "ui_TeamView.h"
#include "Match.h"

enum critere {
    CRIT_ALL = 0, CRIT_PTS_G = 1,
    CRIT_SPE_SSBMEXT = 2, CRIT_SPE_SSBMEXT_REC = 3,
    CRIT_SPE_ACBMEXT = 4, CRIT_SPE_ACBMEXT_REC = 5,
    CRIT_STD = 6, CRIT_DIFF_P = 7, CRIT_MAN = 8
};

class Team : public QFrame {

    public:
        Team(QString const &name, int const id, std::vector<int> const &vectWDLCorr, std::vector<int> const &goals, int const man, int const prevPos, std::vector<int> const ptsPart, std::vector<int> const diffPart, std::vector<int> const vectGFPart, std::vector<int> const awayGFPart, QWidget *parent = 0);
        
        QString getXml() const;
        
        void updateView(QString const &newPosStr, QString const &newDiffPosStr, QString const &newStyleSheetStr);
        void update(int const idAdv = -1, bool const isHost = true, std::map<QString,int> changeMap = std::map<QString,int>(), std::map<QString,int> changeMapAdv = std::map<QString,int>());
        /// @brief Pour mettre a jour LORS DE L'AIDE AU TRI (critere ou le score des matchs entre certaines equipes comptent)
        void maj_sort(std::vector<int> const &idTab);

        /// @part sort's criterions
            static bool pts_g(Team const *a, Team const *b);
            static bool spe_ssBMExt(Team const *a, Team const  *b);
            static bool spe_acBMExt(Team const *a, Team const *b);
            static bool stdd(Team const *a, Team const *b);
            static bool diff_p(Team const *a, Team const *b);
            static bool man(Team const *a, Team const *b);
            static bool man_ghst(Team const *a, Team const *b);

            bool findByName(QString const &nameSeeked);

            bool estEgalA(Team const *b, int const crit);
        /// @endpart sort's criterions
        
        QString getName() const {return _name;}
        int getId() const {return _id;}
        int getMan() const {return _man;}
        int getPrevPos() const {return _prevPos;}
 
        void setMan_Ghst(int const nvMan_Ghst) {_man_ghst = nvMan_Ghst;}
        void setMan(int const nvMan) {_man = nvMan;}
        
        void resetPrevPos(int const nvPrevPos);

    private:
        //Methods
        int getPtsPart(int const id) const;
        int getDiffPart(int const id) const;
        int getBMPart(int const id) const;
        int getBMExtPart(int const id) const;
        //Attributes
        Ui::TeamView ui;
        QString _name;
        int _id;
        int _pts;
        int _MPlyd;
        std::vector<int> _WDLCorr;
        std::vector<int> _goals;
        int _diff;
        int _man;	//pour départager manuellement les egalites...

        int _ptsPartTt;
        int _diffPartTt;
        int _GFPartTt;
        int _awayGFPartTt;
        int _prevPos;  //Pour la difference de position, c'est la position d'origine
        int _man_ghst; //Pour le tri...
        int _BMExt;    //Pour le tri...
        std::vector<int> _ptsPart;
        std::vector<int> _diffPart;
        std::vector<int> _GFPart;
        std::vector<int> _awayGFPart;
};

/// @section Ensemble des foncteurs pour le tri du classement

class Pts_g {
    public:
        bool operator()(Team *a, Team *b) {
            return Team::pts_g(a,b);
        }

};

class Spe_ssBMExt {
    public:
        bool operator()(Team *a, Team *b) {
            return Team::spe_ssBMExt(a,b);
        }
};

class Spe_acBMExt {
    public:
        bool operator()(Team *a, Team *b) {
            return Team::spe_acBMExt(a,b);
        }
};

class Stdd {
    public:
        bool operator()(Team *a, Team *b) {
            return Team::stdd(a,b);
        }
};

class Diff_p {
    public:
        bool operator()(Team *a, Team *b) {
            return Team::diff_p(a,b);
        }
};

class Man {
    public:
        bool operator()(Team *a, Team *b) {
            return Team::man(a,b);
        }
};

/// @endsection

#endif // TEAM_H
