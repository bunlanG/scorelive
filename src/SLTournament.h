/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

/**     @class Représente une compétition
  *
  *     @todo ...
 **/

#ifndef SLTOURNAMENT_H
#define SLTOURNAMENT_H

#include "SLTable.h"
#include "Results.h"

class SLTournament : public QFrame {
    Q_OBJECT

    public:
        SLTournament(QString name, int optHeight, QWidget *parent = 0);
        ~SLTournament();
        
        void clear();
        
        int getOptHeight() const {return _OptHeight;}
        SLTable* getSLTablePointor(int const id = -1) const;
        QString getXml() const;
        bool isModified() const {return _isModified;}
        
        void resetModified() {_isModified = false;}
        void setName(QString const &nwName) {_name = nwName;} 
        void setOptHeight(int const h) {_OptHeight = h;}
        
        void execSLTableWizard(int action);
        void execMatchDayWizard(int action) {_rsltMod->execMatchDayWizard(action, getSLTablePointor());}

    signals:
        void contentsChanged();
        void newMatchDay();

    public slots:
        void addItem(SLTable *item);
        void addItem(Team *item, int const grLink = -1);
        void addItem(Versus *item, int const grLink = -1);
        void addItem(MatchDay *item, int const grLink = -1);
        void addItem(Match *item, int const grLink = -1, int const journee = -1);
        //Set the shown MathcDay when a file is loaded
 
        void setFstJr(int const clsDft, int const jrDft) {_rsltMod->setFstJr(clsDft, jrDft);}

    private slots:
        void changeSLTable(int const nvgrLink);

        void destroySlTable(QObject *obj);
    
    private:
        //Attributes
        Results *_rsltMod;
        std::vector<SLTable*> _vectSLTbl;
        QHBoxLayout *_layout;
        QString _name;
        int _nbr; //Number of SLTable objects
        int _SlTableEna; //Id of the SLTable linked with the shown MatchDay
        int _OptHeight;
        bool _isModified;

        bool showAnotherSLTbl; //Always true, except when clear() is called : say if we need to re-show another SlTable when one is destroyed

        int _nbrSLTable;
        int _nbrTeam;
        int _nbrVersus;
        int _nbrMatchDay;
        int _nbrMatch;

};


#endif // SLTOURNAMENT_H
