/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

/**     @class Conteneur des Match, représente une journée
  *
  *     @todo ...
 **/

#ifndef MATCHDAY_H
#define MATCHDAY_H

#include "Match.h"
#include "SLMatchWiz.h"

class MatchDay : public QFrame {
    Q_OBJECT
    
    public:
        MatchDay(QString const &title, SLTable *slTablePtr, QWidget *parent = 0);
        ~MatchDay();
        
        void addItem(Match *item);
        QString getXml(int const clsLink, int const idLiveGr) const;
        void execWizard(int action, std::vector<QString> matchDayTitles = std::vector<QString>());
        void loadMatchDay(MatchDay const *model);

        //Gettors
        QString title() const;

    signals:
        void contentsChanged();
        void loadMD(int index);
        
    private slots: 
        void slMatchWizClosed(int dialogCode);
     
    private:
        //Methods
        void setupThis();
        void editThis();
        std::vector<QString> getHstsName() const;
        std::vector<QString> getVstsName() const;
        //Attributes
        std::vector<Match*> _vectMatch;
        QVBoxLayout *_layout;
        int _nbr;
        QString _title;
        SLMatchWiz *_wizard;
        SLTable *_slTablePtr;
};

#endif // MATCHDAY_H
