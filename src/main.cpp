/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

/**
  * @todo   !BUGS
  *            - ---
  *
  *         !ENHANCMT
  *            - ...
  *
  *         !TASK
  *            -Masquer les objets inutiles dans les Wizard
  *            -Commenter un peu plus certaines sections
  *				-Angliciser le programme et traduire les strings via Qt Linguist
  *            -Refaire l'indentation de tout le code (passage de 4 espaces par tabulations à 3...)
  *            -Implémenter un système de blocs "try-catch"
  *     
 **/

#include <QtGui>
#include "SLWindow.h"
#include <iostream>

int main(int argc, char* argv[]) {
   QApplication app(argc, argv);
      app.setApplicationName("ScoreLive");
      app.setApplicationVersion("0.99.5");


   qSetMessagePattern("%{type} : %{function} @ %{file}:%{line} - %{message}");

   QTranslator qtTranslator;
      qtTranslator.load("qt_" + QLocale::system().name(), QLibraryInfo::location(QLibraryInfo::TranslationsPath));
      app.installTranslator(&qtTranslator);

   QTranslator scoreLiveTranslator;
      scoreLiveTranslator.load("scoreLive_" + QLocale::system().name());
      app.installTranslator(&scoreLiveTranslator);

   SLWindow window;
      if(argc >= 2)
         window.open(QString(argv[1]));
      window.show();

   return app.exec();
}
