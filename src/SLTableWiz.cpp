/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SLTableWiz.h"
#include "SLTable.h"

SLTableWiz::SLTableWiz(int act, QWidget *parent) : QWizard(parent), _nbr(0), _act(act) {
    setModal(true);

    _lytTeam = new QVBoxLayout();
    _lytVersus = new QVBoxLayout();
    _lytEdition = new QVBoxLayout();

    _title = new QLineEdit();
        _title->setPlaceholderText(tr("Par défaut : \"Classement :\" [Ligue] ou \"Qualifiés :\" [Elimination Directe]"));

    _type = new QGroupBox(tr("Type de groupe"));
        _isRndRob = new QRadioButton(tr("Ligue"));
            _isRndRob->setChecked(true);
        _isKnkOut = new QRadioButton(tr("Elimination directe"));
            _rulAwayGls = new QCheckBox(tr("Règle des buts à l'extérieur"));

    if(act == GROUPE_NEW) {
        setPage(Page_IntroGr, nwIntroPage());
        setPage(Page_Type, nwTypePage());
        setPage(Page_InfosPoule, nwInfosRndRobPage());
        setPage(Page_InfosCoupe, nwInfosKnkOutPage());
        setPage(Page_Equipe, nwTeamsPage());
        setPage(Page_Color, nwColorPage());
        setPage(Page_Versus, nwVersusPage());

        setStartId(Page_IntroGr);

        setWindowTitle(tr("Groupe : création"));
    }
    if(act == GROUPE_EDIT) {
        setPage(Page_EditGr, edSLTablePage());
        setPage(Page_Color, nwColorPage());

        setStartId(Page_EditGr);

        setWindowTitle(tr("Groupe : édition"));
    }
}

SLTableWiz::~SLTableWiz() {
    delete _lytTeam;
    delete _lytVersus;
    delete _lytEdition;

    delete _title;
    delete _isRndRob;
    delete _isKnkOut;
    delete _rulAwayGls;
    delete _type;
}

QWizardPage* SLTableWiz::nwIntroPage() {
    QWizardPage *page = new QWizardPage(this);
        page->setTitle(tr("Introduction"));
    QVBoxLayout *layout = new QVBoxLayout(page);
        page->setLayout(layout);

    QLabel *description = new QLabel(tr("Cet assistant va vous permettre de configurer les équipes pour le classement."), this);
        description->setWordWrap(true);
        layout->addWidget(description);

    return page;
}

QWizardPage* SLTableWiz::nwTypePage() {
    QWizardPage *page = new QWizardPage(this);
        page->setTitle(tr("Informations (1/2)"));
        page->setSubTitle(tr("Type de groupe"));
    QVBoxLayout *layout = new QVBoxLayout(page);
        page->setLayout(layout);

    QFrame *title = new QFrame(page);
        layout->addWidget(title);
    QHBoxLayout *titleLyt = new QHBoxLayout(page);
        title->setLayout(titleLyt);
    QLabel *label0 = new QLabel(tr("En-tête : "), page);
        label0->setMaximumWidth(120); 
        titleLyt->addWidget(label0);
    //_title
        titleLyt->addWidget(_title);
    
    //_type
        _type->move(5, 5);
        layout->addWidget(_type);
    QVBoxLayout *typeLyt = new QVBoxLayout(_type);
        _type->setLayout(typeLyt);
    //_isRndRob
        typeLyt->addWidget(_isRndRob);
    //_isKnkOut
        typeLyt->addWidget(_isKnkOut);

    return page;
}

QWizardPage* SLTableWiz::nwInfosRndRobPage() {
    QWizardPage *page = new QWizardPage(this);
        page->setTitle(tr("Informations (2/2)"));
        page->setSubTitle(tr("Nombre d'équipes & critères de classement"));
    QVBoxLayout *layout = new QVBoxLayout(page); 
        page->setLayout(layout);

    QFrame *nbrTeam = new QFrame(page);
        layout->addWidget(nbrTeam);
    QHBoxLayout *frame0Lyt = new QHBoxLayout(nbrTeam);
        nbrTeam->setLayout(frame0Lyt);
 
    QLabel *label0 = new QLabel(tr("Nombre d'équipes : "), page);
        label0->setMaximumWidth(120);
        frame0Lyt->addWidget(label0);
    QSpinBox *nombre = new QSpinBox(page);
        nombre->setMinimum(0);
        nombre->setMaximumWidth(50);
        QObject::connect(nombre, SIGNAL(valueChanged(int)), this, SLOT(setNbrItem(int)));
        frame0Lyt->addWidget(nombre);

    QGroupBox *criterions = new QGroupBox(tr("Critères :"), page);
        layout->addWidget(criterions); 
    QVBoxLayout *vbox = new QVBoxLayout(criterions);
        criterions->setLayout(vbox);
    for(int i = 0 ; i < 5 ; i++) {
        QFrame *critItem = new QFrame(page);
        uiCriterionBody.push_back(Ui::SLTableWizCriterionItem());
        uiCriterionBody[i].setupUi(critItem);
        uiCriterionBody[i].prop->setText(tr("Critère nÂ°")+QString::number(i+1));
        vbox->addWidget(critItem);
    }

    return page;
}

QWizardPage* SLTableWiz::nwInfosKnkOutPage() {
    QWizardPage *page = new QWizardPage(this);
        page->setTitle(tr("Informations (2/2)"));
        page->setSubTitle(tr("Nombre de versus & critères de qualification"));
 
    QVBoxLayout *layout = new QVBoxLayout(page);
        page->setLayout(layout);

    QFrame *nbrVersus = new QFrame(page);
        layout->addWidget(nbrVersus);
    QHBoxLayout *hbox = new QHBoxLayout(nbrVersus);
        nbrVersus->setLayout(hbox);
    QLabel *label0 = new QLabel(tr("Nombre de versus : "), page);
        label0->setMaximumWidth(120);
        hbox->addWidget(label0);
    QSpinBox *nombre = new QSpinBox(page);
        nombre->setMinimum(0);
        nombre->setMaximumWidth(50);
        QObject::connect(nombre, SIGNAL(valueChanged(int)), this, SLOT(setNbrItem(int)));
        hbox->addWidget(nombre);

    //_rulWayGls
        layout->addWidget(_rulAwayGls);

    return page;
}

QWizardPage* SLTableWiz::nwTeamsPage() {
    QWizardPage *page = new QWizardPage(this);
        page->setTitle(tr("Equipes"));
        page->setSubTitle(tr("Propriétés des équipes"));
    //_lytTeam
        page->setLayout(_lytTeam);

    QFrame *head = new QFrame(page);
        uiTeamHeader.setupUi(head);
        _lytTeam->addWidget(head);

    return page;
}

QWizardPage* SLTableWiz::nwColorPage() {
    QWizardPage *page = new QWizardPage(this);
        page->setTitle(tr("Mise en forme"));
        page->setSubTitle(tr("Coloration des fonds du classement en fonction de la position (pour les poules uniquement)."));
    QVBoxLayout *layout = new QVBoxLayout(page);
        page->setLayout(layout);

    QString color = "";
    QString colorStyleSheet = "";
    
    for(int i = 0 ; i < 7 ; i++) {
        if(i == 0) {
            color = tr("Bleu 1");
            colorStyleSheet = "#000080";
        }
        if(i == 1) {
            color = tr("Bleu 2");
            colorStyleSheet = "#3030C0";
        }
        if(i == 2) {
            color = tr("Vert 1");
            colorStyleSheet = "#008000";
        }
        if(i == 3) {
            color = tr("Vert 2");
            colorStyleSheet = "#30C030";
        }
        if(i == 4) {
            color = tr("Gris");
            colorStyleSheet = "#707070";
        }
        if(i == 5) {
            color = tr("Rouge 1");
            colorStyleSheet = "#800000";
        }
        if(i == 6) {
            color = tr("Rouge 2");
            colorStyleSheet = "#C03030";
        }

        QFrame *frame = new QFrame(page);
            uiColorBody.push_back(Ui::SLTableWizColorItem());
            uiColorBody[i].setupUi(frame);
            uiColorBody[i].color->setText(color);
            uiColorBody[i].color->setStyleSheet("color: rgb(255, 255, 255);\n"
                                                "background-color: "+colorStyleSheet+";");
            layout->addWidget(frame);
    }

    return page;
}

QWizardPage* SLTableWiz::nwVersusPage() {
    QWizardPage *page = new QWizardPage(this);
        page->setTitle(tr("Versus"));
        page->setSubTitle(tr("Propriétés des versus"));
    //_lytVersus
        page->setLayout(_lytVersus);

    QFrame *head = new QFrame(page);
        uiVersusHeader.setupUi(head);
        _lytVersus->addWidget(head);

    return page;
}

QWizardPage *SLTableWiz::edSLTablePage() {
    QWizardPage *page = new QWizardPage(this);
        page->setTitle(tr("Edition"));
        page->setSubTitle(tr("En-tête et critère manuel"));
    //_lytEdition
        page->setLayout(_lytEdition);

    QFrame *title = new QFrame(page);
        _lytEdition->addWidget(title);
    QHBoxLayout *hbox0 = new QHBoxLayout(page); 
        title->setLayout(hbox0);
 
    QLabel *label0 = new QLabel(tr("En-tête : "), page);
        label0->setMaximumWidth(120);
        hbox0->addWidget(label0);
    //_title
        hbox0->addWidget(_title);

    return page;
}

void SLTableWiz::setEdPage(int type, QString title, std::vector<QString> names, std::vector<int> mans, std::vector<std::vector<int> > bckGrdColor) {
    if(type == SLTable::Type_RoundRobin)
        _isRndRob->setChecked(true);
    else if(type == SLTable::Type_KnockOut)
        _isKnkOut->setChecked(true);

    //title
    _title->setText(title);

    if(getType() == SLTable::Type_RoundRobin) {
        //names + mans
        for(int i = 0 ; i < names.size() ; i++) {
            QFrame *ass = new QFrame(this);
            uiEditionBody.push_back(Ui::SLTableWizTeamEditItem());
            uiEditionBody[i].setupUi(ass);
                uiEditionBody[i].id->setText(QString::number(i+1));
                uiEditionBody[i].nom->setText(names[i]);
                uiEditionBody[i].man->setValue(mans[i]);
            _SLTableItems.push_back(ass);
            _lytEdition->addWidget(ass);
        }

        //minGraph + maxGraph
        for(int i = 0 ; i < uiColorBody.size() ; i++) {
            uiColorBody[i].min->setValue(0);
            uiColorBody[i].max->setValue(0);
        }

        for(int i = 0 ; i < bckGrdColor.size() ; i++) {
            if(bckGrdColor[i][2] == 0x000080) {
                uiColorBody[0].min->setValue(bckGrdColor[i][0]);
                uiColorBody[0].max->setValue(bckGrdColor[i][1]);
            } else if(bckGrdColor[i][2] == 0x3030C0) {
                uiColorBody[1].min->setValue(bckGrdColor[i][0]);
                uiColorBody[1].max->setValue(bckGrdColor[i][1]);
            } else if(bckGrdColor[i][2] == 0x008000) {
                uiColorBody[2].min->setValue(bckGrdColor[i][0]);
                uiColorBody[2].max->setValue(bckGrdColor[i][1]);
            } else if(bckGrdColor[i][2] == 0x30C030) {
                uiColorBody[3].min->setValue(bckGrdColor[i][0]);
                uiColorBody[3].max->setValue(bckGrdColor[i][1]);
            } else if(bckGrdColor[i][2] == 0x707070) {
                uiColorBody[4].min->setValue(bckGrdColor[i][0]);
                uiColorBody[4].max->setValue(bckGrdColor[i][1]);
            } else if(bckGrdColor[i][2] == 0x800000) {
                uiColorBody[5].min->setValue(bckGrdColor[i][0]);
                uiColorBody[5].max->setValue(bckGrdColor[i][1]);
            } else if(bckGrdColor[i][2] == 0xC03030) {
                uiColorBody[6].min->setValue(bckGrdColor[i][0]);
                uiColorBody[6].max->setValue(bckGrdColor[i][1]);
            }
        }
    }
}

void SLTableWiz::setNbrItem(int nwNbrItem) {
    bool valueChangedPlus = (_nbr < nwNbrItem); //true : on ajoute des equipes-versus ; false : on en enleve

    if (nwNbrItem != 0) //Evite d'avoir un melange d'equipe et de versus (melange poule / coupe) ^^'
        _type->setEnabled(false);
    else
        _type->setEnabled(true);

    if(valueChangedPlus) {
        for (int i = _nbr; i < nwNbrItem; i++) { //on ajoute des equipes-versus
            if(getType() == SLTable::Type_RoundRobin) {
                QFrame *ass = new QFrame(this);
                uiTeamBody.push_back(Ui::SLTableWizTeamItem());
                uiTeamBody[i].setupUi(ass);
                uiTeamBody[i].id->setText(QString::number(i+1));
                _SLTableItems.push_back(ass);
                _lytTeam->addWidget(ass);
            } else {
                QFrame *ass2 = new QFrame(this);
                uiVersusBody.push_back(Ui::SLTableWizVersusItem());
                uiVersusBody[i].setupUi(ass2);
                uiVersusBody[i].id->setText(QString::number(i+1));
                _SLTableItems.push_back(ass2);
                _lytVersus->addWidget(ass2);
            }
        }
    } else {
        for (int i = _nbr; i > nwNbrItem; i--) { //on enleve des equipes-versus
            if(getType() == SLTable::Type_RoundRobin) {
                _lytTeam->removeWidget(_SLTableItems[i-1]);
                _SLTableItems[i-1]->hide();

                delete _SLTableItems[i];
                _SLTableItems.pop_back();

                uiTeamBody.pop_back();
            } else {
                _lytVersus->removeWidget(_SLTableItems[i-1]);
                _SLTableItems[i-1]->hide();

                delete _SLTableItems[i];
                _SLTableItems.pop_back();

                uiVersusBody.pop_back();
            }
        }
    }

    _nbr = nwNbrItem;
}

/// @reimp
int SLTableWiz::nextId() const {
    //Progression :
    //Poule : Intro -> Type -> Info_Poule -> Equipe -> Color
    //Coupe : Intro -> Type -> Info_Coupe -> Versus

    switch (currentId()) {
        case Page_IntroGr:
            return Page_Type;
        break;
        case Page_Type:
            if(getType() == SLTable::Type_RoundRobin)
                return Page_InfosPoule;
            else
                return Page_InfosCoupe;
        break;
        case Page_InfosPoule:
            return Page_Equipe;
        break;
        case Page_InfosCoupe:
            return Page_Versus;
        break;
        case Page_Equipe:
            return Page_Color;
        break;
        case Page_EditGr:
            if(getType() == SLTable::Type_RoundRobin)
                return Page_Color;
            else
                return -1;
        break;
        case Page_Color: case Page_Versus: default:
            return -1;
        break;
    }
}

int SLTableWiz::getAction() const {
    return _act;
}

int SLTableWiz::getType() const {
    if(_isRndRob->isChecked())
        return SLTable::Type_RoundRobin;
    else if(_isKnkOut->isChecked())
        return SLTable::Type_KnockOut;
    else
        return -1;
}

QString SLTableWiz::getTitle() const {
    return _title->text();
}


std::vector< std::vector<int> > SLTableWiz::getBckGrdColor() const {
    std::vector< std::vector<int> > matr;
    std::vector<int> vectTmp;
    int minGrph;
    int maxGrph;
    int color;
        
    for(int i = 0 ; i < 7 ; i++) {
        minGrph = uiColorBody[i].min->value();
        maxGrph = uiColorBody[i].max->value();
        if(minGrph > 0 && maxGrph > 0) {
            if(i == 0)
                color = 0x000080;
            else if(i == 1)
                color = 0x3030C0;
            else if(i == 2)
                color = 0x008000;
            else if(i == 3)
                color = 0x30C030;
            else if(i == 4)
                color = 0x707070;
            else if(i == 5)
                color = 0x800000;
            else if(i == 6)
                color = 0xC03030;
           
            vectTmp.push_back(minGrph);
            vectTmp.push_back(maxGrph);
            vectTmp.push_back(color);
            matr.push_back(vectTmp);
            vectTmp.clear();
        }
    }
    
    return matr;
}

std::vector<int> SLTableWiz::getCriterions() const {
    std::vector<int> vect;
    
    for(int i = 0 ; i < uiCriterionBody.size() ; i++) {
        int id = uiCriterionBody[i].criterion->currentIndex();
        if(id != 0)
            vect.push_back(id);
    }
    
    return vect;
}

std::vector<QString> SLTableWiz::getTeamsName() const {
    std::vector<QString> vect;

    for(int i = 0 ; i < uiTeamBody.size() ; i++) {
       vect.push_back(uiTeamBody[i].name->text());

    }

    return vect;
}

std::vector< std::vector<QString> > SLTableWiz::getTeamsNames() const {
    std::vector< std::vector<QString> > matr;
    std::vector<QString> vectTmp;

    for(int i = 0 ; i < uiVersusBody.size() ; i++) {
        vectTmp.clear();

        vectTmp.push_back(uiVersusBody[i].team1->text());
        vectTmp.push_back(uiVersusBody[i].team2->text());

        matr.push_back(vectTmp);
    }

    return matr;
}

std::vector<int> SLTableWiz::getMans() const {
    std::vector<int> vect;

    for(int i = 0 ; i < uiEditionBody.size() ; i++) {
        vect.push_back(uiEditionBody[i].man->value());
    }

    return vect;
}

