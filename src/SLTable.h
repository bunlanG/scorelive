/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

/**     @class Représente un groupe, pouvant etre de type poule(rencontres avec un classement) ou de type coupe(matches à éliminations directes)
  *
  *     @todo ...
 **/

#ifndef SLTABLE_H
#define SLTABLE_H

#include "Team.h"
#include "Versus.h"
#include "SLExpressions.h"
#include "Match.h"
#include "SLTableWiz.h"

class SLTable : public QFrame {
    Q_OBJECT

    public:
        enum typeSLTable { Type_RoundRobin, Type_KnockOut };
        
        ///Default Constructor
        SLTable(QWidget *parent = 0);
        /// Round-Robin Table Constructor
        SLTable(QString const &name, QString const &title, std::vector< std::vector<int> > const &bckGrdColor, std::vector<int> const &criterions, QWidget *parent = 0);
        /// KnockOut Table Constructor
        SLTable(QString const &name, QString const &title, bool const rulAwayGls, QWidget *parent = 0);
        ~SLTable();

        void addItem(Team *item); //Ajoute une equipe au classement (Type_RoundRobin)
        void addItem(Versus *item); //Ajoute un versus au classement (Type_KnockOut)
        void execWizard(int action);
        
        std::vector< std::vector<int> > getBckGrdColor() const {return _bckGrdColor;}
        QString getHead() const {return _title;}
        //PRIVATE
        std::vector<int> getMans() const;
        int getOptHeight() const {return _OptHeight;}
        //PRIVATE
        std::vector<QString> getTeams() const;
        int getType() const {return _type;}
        QString getXml(int const clsLink) const;
        
        //PRIVATE
        void editGroupe(QString const &title, std::vector< std::vector<int> > const &bckGrdColor, std::vector<int> const &mans);
        void setOptHeight(int const h) {_OptHeight = h;}
        

    public slots:
        void isFini(Match *matchSender, QString const &equipe1, QString const &equipe2) const;
        void update(QString const &hstName, QString const &vstName, std::map<QString,int> const &changeMapHst, std::map<QString,int> const &changeMapVst);

    signals:
        void contentsChanged();
        void newMatchDay();

    private slots:
        void resetPos();
        void slTableWizClosed(int dialogCode); 

    private:
      //Methods
        void setupThis();
        void setupForRoundRobin(QString const &name, QString const &title, std::vector< std::vector<int> > const &bckGrdColor, std::vector<int> const &criterions);
        void setupForKnockOut(QString const &name, QString const &title, bool const rulAwayGls);
        void setThis();
        
        void updateView();
        void setBckGrdColorTeams(int position);
        /// Pour trier selon le critere _criterions[i_crit]
        void sortTeams(int idBeg, int idEnd, int i_crit);
      //Attributes
        /// Round-Robin only
        Ui::TeamView uiRndRobHead;
        std::vector<Team*> _rndRobVect;
        std::vector< std::vector<int> > _bckGrdColor;
        /// KnockOut only
        std::vector<Versus*> _knkOutVect;
        bool _rulAwayGls;
        /// Both
        std::vector<int> _criterions;           //Tableau des criteres de tri
        QLabel *_header;
        QSpacerItem *_spacer;
        QVBoxLayout *_layout;
        SLTableWiz *_wizard;
        int _nbr;
        QString _name;
        QString _title;
        int _type;
        int _OptHeight;
};

#endif // SLTABLE_H
