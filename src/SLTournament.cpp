/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SLTournament.h"
#include <QtDebug>

SLTournament::SLTournament(QString name, int optHeight, QWidget *parent) : QFrame(parent), _name(name), _nbr(0), _SlTableEna(0), _OptHeight(optHeight), _isModified(false), _nbrSLTable(0), _nbrTeam(0), _nbrVersus(0), _nbrMatchDay(0), _nbrMatch(0) {
   showAnotherSLTbl = true;
   setStyleSheet(QString::fromUtf8("background-color: rgb(90, 90, 90);"));

    _layout = new QHBoxLayout();
        _layout->setSpacing(0);
        _layout->setContentsMargins(0, 0, 0, 0);
        _layout->setSizeConstraint(QLayout::SetMinimumSize);
        setLayout(_layout);
    _rsltMod = new Results();
        QObject::connect(_rsltMod, SIGNAL(contentsChanged()), this, SIGNAL(contentsChanged()));
        QObject::connect(_rsltMod, SIGNAL(changeSLTable(int)), this, SLOT(changeSLTable(int)));
        _layout->addWidget(_rsltMod);
    _layout->addItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));

    setWindowTitle(_name);
}

SLTournament::~SLTournament() {
    clear();

    delete _layout;
    delete _rsltMod;
}

void SLTournament::clear() {
   showAnotherSLTbl = false;
    for(int i = 0 ; i < _vectSLTbl.size() ; i++)
        delete _vectSLTbl[i];
    _vectSLTbl.clear();

    _rsltMod->clear();

    setMinimumHeight(0);

    _nbr = 0;
    _SlTableEna = 0;

    _nbrSLTable = 0; _nbrTeam = 0; _nbrVersus = 0; _nbrMatchDay = 0; _nbrMatch = 0;
    showAnotherSLTbl = true;
}

QString SLTournament::getXml() const {
    QString text = "";

    text += "<SLTRoot fileVersion=\"SLTv1\" >\n";
    
    text += QString("\t<Infos fileName=\"%1\" optHeight=\"%2\" nbrSLTable=\"%3\" nbrTeam=\"%4\" nbrVersus=\"%5\" nbrMatchDay=\"%6\" nbrMatch=\"%7\" />\n")
            .arg(_name).arg(_OptHeight).arg(_nbrSLTable).arg(_nbrTeam).arg(_nbrVersus).arg(_nbrMatchDay).arg(_nbrMatch);

    text += "\t<Content>\n"
            "\t\t<SLTables>\n";
    for (int i = 0 ; i < _nbr ; i++) {
        text += _vectSLTbl[i]->getXml(i); //RndRob | KnkOut
    }
    text += "\t\t</SLTables>\n";
    text += _rsltMod->getXml(); //Results
    text += "\t</Content>\n";
    
    text += "</SLTRoot>\n";

    return text;
}

SLTable* SLTournament::getSLTablePointor(int const id) const {
    if(id >= 0 && id < _nbr)
        return _vectSLTbl[id];
    else if(id == -1)
        return _vectSLTbl[_SlTableEna];
    else
        return 0;
}

void SLTournament::addItem(SLTable *item) {
    if (_nbr > 0)
        _vectSLTbl[_SlTableEna]->hide();

    _nbr++;
    _SlTableEna = _nbr-1;
    _vectSLTbl.push_back(item);
    QObject::connect(item, SIGNAL(contentsChanged()), this, SIGNAL(contentsChanged()));
    QObject::connect(item, SIGNAL(newMatchDay()), this, SIGNAL(newMatchDay()));
    QObject::connect(item, SIGNAL(destroyed(QObject*)), this, SLOT(destroySlTable(QObject*)));

    _layout->addWidget(item);

    _rsltMod->ajGroupe();

    _isModified = true;
    _nbrSLTable++;
}

void SLTournament::addItem(Team *item, int const grLink) {
    if(grLink >= 0 && grLink < _nbr)
        _vectSLTbl[grLink]->addItem(item);
    else
        _vectSLTbl[_nbr-1]->addItem(item);

    _isModified = true;
    _nbrTeam++;
}

void SLTournament::addItem(Versus *item, int const grLink) {
    if(grLink >= 0 && grLink < _nbr)
        _vectSLTbl[grLink]->addItem(item);
    else
        _vectSLTbl[_nbr-1]->addItem(item);

    _isModified = true;
    _nbrVersus++;
}

void SLTournament::addItem(MatchDay *item, int const grLink) {
   _rsltMod->addItem(item, grLink);

   _isModified = true;
   _nbrMatchDay++;
}

void SLTournament::addItem(Match *item, int const grLink, int const journee) {
   _rsltMod->addItem(item, grLink, journee);

   _isModified = true;
   _nbrMatch++;
}

void SLTournament::destroySlTable(QObject *obj) {
   if(showAnotherSLTbl && &(*(_vectSLTbl[_SlTableEna])) == &(*(obj))) {
      _vectSLTbl[_SlTableEna]->hide();

      //Dernier groupe :
      if(_SlTableEna = _nbr - 1) {
         _vectSLTbl.pop_back();
         _nbr--;
         //On affiche le groupe précédent (s'il existe)
         _SlTableEna -= 1;
         if(_SlTableEna >= 0) {
            changeSLTable(_SlTableEna);
            _rsltMod->delLastSlTable();
         } else {
            _SlTableEna = 0;
         }
      }
   }
}

void SLTournament::execSLTableWizard(int action) {
    if(action == GROUPE_NEW) {
        SLTable *newSLTable = new SLTable();
        addItem(newSLTable);
        newSLTable->execWizard(action);
    } else if(action == GROUPE_EDIT) {
        _vectSLTbl[_SlTableEna]->execWizard(action);
    }
}

void SLTournament::changeSLTable(int const nvgrLink) {
    _vectSLTbl[_SlTableEna]->hide();

    _SlTableEna = nvgrLink;

    _vectSLTbl[_SlTableEna]->show();

    _isModified = true;
}
