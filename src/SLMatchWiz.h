/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

/**    @class Assistant qui ajoute de nouvelles jounées
  *
  *    @todo ...
 **/

#ifndef SLMATCHWIZ_H
#define SLMATCHWIZ_H

#include <QtWidgets/QWizard>
#include <QtWidgets/QMessageBox>
#include "Match.h"
#include "ui_SLMatchWizHead.h"
#include "ui_SLMatchWizItem.h"
#include "ui_SLMatchWizEditHead.h"
#include "ui_SLMatchWizEditItem.h"

enum AssMatchAction {
    MATCHS_NEW, MATCHS_EDIT
};

/// @enum pages' id [\see \reimp of nextId()]
enum {
    Page_IntroMt, Page_Infos, Page_Match,
    Page_EditMt
};


class SLMatchWiz : public QWizard {
    Q_OBJECT

    public:
        SLMatchWiz(int act, QWidget *parent = 0);
        void setNwMatchPage(std::vector<QString> const &matchDayTitles);
        void setEdPage(QString const &title, std::vector<QString> const &nomLocal, std::vector<QString> const &nomVisiteur);
        void loadMatchDay(std::vector<QString> const &hstNames, std::vector<QString> const &vstNames);

        //Gettors
        int getAction() const;
        int getNbrMt() const;
        QString getMatchDayName() const;
        std::vector<int> getOrdre() const;
        std::vector<bool> getNoDraws() const;
        std::vector< std::vector<QString> > getTeamsNames() const;
        
    signals:
        void loadMD(int index);
     
    private slots:
        void setNbrMt(int nvNbrMt);
        void loadMatchDayEmit();
        void swapTeams();

    private:
        //Méthods
        QWizardPage *nvIntroPage();
        QWizardPage *nvInfosPage();
        QWizardPage *nvMatchPage();
        QWizardPage *edMatchsPage();
        int nextId() const;
        //Attributes
        Ui::SLMatchWizHead _uiHeader;
        int _nbr;
        int _act;
        QVBoxLayout* _lyt;
        QVBoxLayout* _lytEdit;
        std::vector<Ui::SLMatchWizItem> _vectUiMatch;
        std::vector<Ui::SLMatchWizEditItem> _vectUiEditMt;
        std::vector<QFrame*> _vectMatch;
        QLineEdit *_name;
        QSpinBox *_nbrMt;
};

#endif // SLMATCHWIZ_H
