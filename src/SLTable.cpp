/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SLTable.h"

//Default Constructor
SLTable::SLTable(QWidget *parent) : QFrame(parent), _wizard(0),  _spacer(0), _nbr(0) {
    _layout = new QVBoxLayout(this);
    _layout->setSizeConstraint(QLayout::SetMinimumSize);
    
    _name = "";
    _title = "";
}

// Round-Robin constructor
SLTable::SLTable(QString const &name, QString const &title, std::vector< std::vector<int> > const &bckGrdColor, std::vector<int> const &criterions, QWidget *parent) : QFrame(parent), _wizard(0), _spacer(0), _nbr(0), _OptHeight(0) {
    _layout = new QVBoxLayout(this);
    _layout->setSizeConstraint(QLayout::SetMinimumSize);
    
    setupForRoundRobin(name, title, bckGrdColor, criterions);
}
 
// KnockOut constructor
SLTable::SLTable(QString const &name, QString const &title, bool const rulAwayGls, QWidget *parent) : QFrame(parent), _wizard(0), _spacer(0), _nbr(0), _OptHeight(0) {
    _layout = new QVBoxLayout(this);
    _layout->setSizeConstraint(QLayout::SetMinimumSize);
    
    setupForKnockOut(name, title, rulAwayGls);
}

SLTable::~SLTable() {
    delete _wizard;
}

void SLTable::setupForRoundRobin(QString const &name, QString const &title, std::vector< std::vector<int> > const &bckGrdColor, std::vector<int> const &criterions) {
    _name = name;
    _title = title;
    _bckGrdColor = bckGrdColor;
    _criterions = criterions; 
    _type = SLTable::Type_RoundRobin;
    
    if (title == "")
        _title = tr("Classement :");

    //setup the header with the title
    _header = new QLabel(_title, this);

    setupThis();

    _layout->addWidget(_header);

    //I add a button to reset the differential position
    QPushButton *resetBut = new QPushButton(tr("Reset"), _header);
        resetBut->setCursor(QCursor(Qt::PointingHandCursor));
        resetBut->move(10,0);
        resetBut->setStyleSheet("QPushButton {\n"
                                "font: 75 8pt \"MS Shell Dlg 2\";\n"
                                "color : rgb(255, 255, 255);\n"
                                "background-color: rgb(40, 40, 40);\n"
                                "}\n"
                                "QPushButton:hover {\n"
                                "font: 100 8pt \"MS Shell Dlg 2\";\n"
                                "color : rgb(200, 200, 200);\n"
                                "background-color: rgb(75, 75, 75);\n"
                                "}\n"
                                "QPushButton:pressed {\n"
                                "color : rgb(60, 255, 255);\n"
                                "background-color: rgb(10, 75, 75);\n"
                                "}");
    QObject::connect(resetBut, SIGNAL(clicked()), this, SLOT(resetPos()));

    //Round-robin's header
    QFrame *headerTbl = new QFrame(this);
        uiRndRobHead.setupUi(headerTbl);
        _layout->addWidget(headerTbl);

    _layout->addItem(_spacer);
}

void SLTable::setupForKnockOut(QString const &name, QString const &title, bool const rulAwayGls) {
    _name = name;
    _title = title;
    _rulAwayGls = rulAwayGls;
    _type = SLTable::Type_KnockOut;
    
    //Set a default title if empty
    if (title == "")
        _title = tr("Qualifiés :");
    _header = new QLabel(_title, this);

    setupThis();

    _layout->addWidget(_header);

    _layout->addItem(_spacer);
}

// Common setup for KnockOut and Round-Robin Table
void SLTable::setupThis() {
    //_spacer permit to keep the same height whatever the window's size (no autodimension)
    //_spacer is always the last of _layout
    if(_spacer == 0)
        _spacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
    
    setFrameStyle(QFrame::Panel | QFrame::Plain);
    setLineWidth(1);
    setStyleSheet(QString::fromUtf8("background-color: rgb(90, 90, 90);"));
    
    _layout->setSpacing(1);
    _layout->setContentsMargins(0, 0, 0, 1);
    setLayout(_layout);
    
    QFont font;
        font.setItalic(true);
        font.setBold(true);
        font.setFamily("Nokia Pure Text");
        font.setPointSize(8); 
    
    _header->setFont(font);
    _header->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    _header->setMinimumHeight(22);
    _header->setMaximumHeight(22);
    _header->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
                                              "background-color: rgb(70, 70, 70);")); 
}

void SLTable::addItem(Team *item) {
    if(_type == SLTable::Type_RoundRobin) {
        _nbr++;

        _rndRobVect.push_back(item);

        _layout->removeItem(_spacer);
        _layout->addWidget(item);
        _layout->addItem(_spacer);

        //background color
        setBckGrdColorTeams(_nbr);

        updateView();
    }
}

void SLTable::addItem(Versus *item) {
    if(_type == SLTable::Type_KnockOut) {
        QFont font;
            font.setBold(true);
            font.setItalic(true); 
        
        item->setStyleSheet(QString::fromUtf8("color: rgb(0, 255, 0);\n" 
                                              "background-color: rgb(40, 40, 40);"));
        item->setFont(font);
        item->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
        item->setMinimumSize(402, 22);
        item->setMaximumSize(402, 22);
        _layout->removeItem(_spacer);
        _layout->addWidget(item);
        _layout->addItem(_spacer);
        
        _knkOutVect.push_back(item);
            _nbr++; 

        updateView();
    }
}

void SLTable::execWizard(int action) {
    _wizard = new SLTableWiz(action);
    QObject::connect(_wizard, SIGNAL(finished(int)), this, SLOT(slTableWizClosed(int)));
    
    if(action == GROUPE_NEW) {
    } else if(action == GROUPE_EDIT) {
        _wizard->setEdPage(_type, _title, getTeams(), getMans(), _bckGrdColor);
    }
    
    _wizard->show();
}

void SLTable::slTableWizClosed(int dialogCode) {
   int action = _wizard->getAction();

   if(dialogCode == QDialog::Accepted) {
      emit contentsChanged();

      if(action == GROUPE_NEW) {
         setThis();
         emit newMatchDay();

         //Add all items
         int nbr = _wizard->getNbrItems();
         int typeTable = _wizard->getType();

         if(typeTable == SLTable::Type_RoundRobin) {
            std::vector<int> criterions = _wizard->getCriterions();
            std::vector< std::vector<int> > bckGrdColor = _wizard->getBckGrdColor();
            std::vector<QString> teamsName = _wizard->getTeamsName();

            std::vector<int> vectInit(nbr, 0);
            std::vector<int> vectWDLCorr(4, 0);
            std::vector<int> goals(2, 0);
            std::vector<int> ptsPart;
            std::vector<int> diffPart;
            std::vector<int> vectGFPart;
            std::vector<int> awayGFPart;

            for (int i = 0; i < nbr; i++) {
               if(std::find(criterions.begin(), criterions.end(), 4) != criterions.end() || std::find(criterions.begin(), criterions.end(), 5) != criterions.end()) {
                  ptsPart = vectInit; diffPart = vectInit; vectGFPart = vectInit; awayGFPart = vectInit;
               } else if(std::find(criterions.begin(), criterions.end(), 2) != criterions.end() || std::find(criterions.begin(), criterions.end(), 3) != criterions.end()) {
                  ptsPart = vectInit; diffPart = vectInit; vectGFPart = vectInit;
               } else if(std::find(criterions.begin(), criterions.end(), 7) != criterions.end()) {
                  diffPart = vectInit;
               }

               addItem(new Team(teamsName[i], i, vectWDLCorr, goals, 0, i,  ptsPart, diffPart, vectGFPart, awayGFPart));

               ptsPart.clear();
               diffPart.clear();
               vectGFPart.clear();
               awayGFPart.clear();
            }
         } else if(typeTable == SLTable::Type_KnockOut) {

            for (int i = 0; i < nbr; i++) {
               std::vector<int> diffs(3, 0);
               std::vector< std::vector<QString> > teamsNames = _wizard->getTeamsNames();

               addItem(new Versus(teamsNames[i], diffs));
            }
         }
      } else if(action == GROUPE_EDIT) {
         editGroupe(_wizard->getTitle(), _wizard->getBckGrdColor(), _wizard->getMans());
      }
   } else if (action == GROUPE_NEW) {
      //SlTable non initialisée : suppression par le widget parent
      deleteLater();
   }

   delete _wizard;
   _wizard = 0;

   updateView();
}

void SLTable::setThis() {
    if(_wizard->getAction() == GROUPE_NEW) {
        if(_wizard->getType() == SLTable::Type_RoundRobin) {
            setupForRoundRobin("NoNameYet", _wizard->getTitle(), _wizard->getBckGrdColor(), _wizard->getCriterions());
        } else if(_wizard->getType() == SLTable::Type_KnockOut) {
            setupForKnockOut("NoNameYet", _wizard->getTitle(), _wizard->getRulAwayGls());
        }
    } else if(_wizard->getAction() == GROUPE_EDIT) {
        //Already impl. (yet...)    
    }
}

std::vector<int> SLTable::getMans() const {
    std::vector<int> tab;

    if(_type == SLTable::Type_RoundRobin) {
        for(int i = 0 ; i < _nbr ; i++) {
            tab.push_back(_rndRobVect[i]->getMan());
        }
    }

    return tab;
}

std::vector<QString> SLTable::getTeams() const {
    std::vector<QString> tab;

    if(_type == SLTable::Type_RoundRobin) {
        for(int i = 0 ; i < _nbr ; i++) {
            tab.push_back(_rndRobVect[i]->getName());
        }
    }

    return tab;
}

QString SLTable::getXml(int const clsLink) const {
    QString text = "";
    
    if(_type == SLTable::Type_RoundRobin) {
        text += QString("\t\t\t<RndRob id=\"%1\" name=\"%2\" title=\"%3\" bckGrdClr=\"%4\" criterions=\"%5\" >\n")
               .arg(clsLink).arg(_name).arg(_title).arg(SLExpressions::intMatrToExp(_bckGrdColor)).arg(SLExpressions::intVectToExp(_criterions));
        for (int i = 0 ; i < _nbr ; i++) {
           text += _rndRobVect[i]->getXml(); //Team
        }
        text += "\t\t\t</RndRob>\n";
    } else if(_type == SLTable::Type_KnockOut) {
        int intRulAwayGls = 0;
        if (_rulAwayGls)
            intRulAwayGls = 1;
        
        text += QString("\t\t\t<KnkOut id=\"%1\" name=\"%2\" title=\"%3\" rulAwayGls=\"%4\" >\n")
               .arg(clsLink).arg(_name).arg(_title).arg(intRulAwayGls);
        for (int i = 0 ; i < _nbr ; i++) {
            text += _knkOutVect[i]->getXml(); //Versus
        }
        text += "\t\t\t</KnkOut>\n"; 
    }
    
    return text;
}

void SLTable::editGroupe(const QString &title, const std::vector<std::vector<int> > &bckGrdColor, const std::vector<int> &mans) {
    //title
    _title = title;

    if(_type == SLTable::Type_RoundRobin) {
        _bckGrdColor = bckGrdColor;
        
        //update background color teams
        for(int i = 0 ; i < _nbr ; i++) {
            setBckGrdColorTeams(i+1);
        }

        //man
        for(int i = 0 ; i < _nbr ; i++) {
            _rndRobVect[i]->setMan(mans[i]);
        }

        updateView();
    }
}

void SLTable::isFini(Match *matchSender, QString const &equipe1, QString const &equipe2) const {
    if(_type == SLTable::Type_KnockOut) {
        for(int i = 0 ; i < _knkOutVect.size(); i++) {
            if((equipe1 == _knkOutVect[i]->getEq1() && equipe2 == _knkOutVect[i]->getEq2()) || (equipe1 == _knkOutVect[i]->getEq2() && equipe2 == _knkOutVect[i]->getEq1())) {
                bool isFinish = (_knkOutVect[i]->qualifie(_rulAwayGls) != "...");
                matchSender->mayBeFinish(isFinish);
            }
        }
    }

    matchSender->mayBeFinish(false);
}

void SLTable::update(QString const &hstName, QString const &vstName, std::map<QString,int> const &changeMapHst, std::map<QString,int> const &changeMapVst) {
    if(_type == SLTable::Type_RoundRobin) {
        for (int i = 0 ; i < _nbr ; i++) {
            for(int j = 0 ; j < _nbr ; j++) {
                if (hstName == _rndRobVect[i]->getName() && vstName == _rndRobVect[j]->getName())
                    _rndRobVect[i]->update(_rndRobVect[j]->getId(), true, changeMapHst, changeMapVst);
                if (vstName == _rndRobVect[i]->getName() && hstName == _rndRobVect[j]->getName())
                    _rndRobVect[i]->update(_rndRobVect[j]->getId(), false, changeMapVst, changeMapHst);
            }
        }
    }

    if(_type == SLTable::Type_KnockOut) {
        for (int i = 0 ; i < _nbr ; i++) {
            if(hstName == _knkOutVect[i]->getEq1() && vstName == _knkOutVect[i]->getEq2())
                _knkOutVect[i]->update(true, changeMapHst, changeMapVst);
            if(hstName == _knkOutVect[i]->getEq2() && vstName == _knkOutVect[i]->getEq1())
                _knkOutVect[i]->update(false, changeMapVst, changeMapHst);
        }
    }

    updateView();
}

void SLTable::resetPos() {
    emit contentsChanged();

    for (int i = 0 ; i < _nbr ; i++)
        _rndRobVect[i]->resetPrevPos(i);
}

void SLTable::updateView() {
    if(_type == SLTable::Type_RoundRobin) {
        if(_criterions.size() > 0)
            sortTeams(0, _rndRobVect.size() - 1, 0);
        
        QString newPosStr;
        QString newDiffPosStr;
        QString newStyleSheetStr;
        for (int i = 0 ; i < _nbr ; i++) {
            if(i > 0 && _rndRobVect[i-1]->estEgalA(_rndRobVect[i], CRIT_ALL)) { //egalite qu'on ne peut departager a partir des resultats
                newPosStr = "-";
            } else {
                newPosStr = QString::number(i+1)+"e";
            }

            int n = _rndRobVect[i]->getPrevPos() - i;
            if(n > 0) {
                newDiffPosStr = QString::fromUtf8("\u25B2")+QString::number(n);
                newStyleSheetStr = QString::fromUtf8("color: rgb(0, 255, 0);\n"
                                                     "background-color: rgb(0, 0, 0);");
            } else if (n < 0) {
                newDiffPosStr = QString::fromUtf8("\u25BC")+QString::number(-n);
                newStyleSheetStr = QString::fromUtf8("color: rgb(255, 0, 0);\n"
                                                     "background-color: rgb(0, 0, 0);");
            } else {
                newDiffPosStr = QString::fromUtf8("\u25C4")+QString::fromUtf8("\u25BA");
                newStyleSheetStr = QString::fromUtf8("color: rgb(64, 64, 64);\n"
                                                     "background-color: rgb(0, 0, 0);");
            }
            
            _rndRobVect[i]->updateView(newPosStr, newDiffPosStr, newStyleSheetStr);
            setBckGrdColorTeams(i+1);
        }

        //on enleve TOUS les objets Team du layout
        for(int i = 0 ; i < _nbr ; i++) {
            _layout->removeWidget(_rndRobVect[i]);
        }

        //on remet les objets Match : ils seront dans l'ordre !
        for(int i = 0 ; i < _nbr ; i++) {
            _layout->addWidget(_rndRobVect[i]);
        }

        _layout->removeItem(_spacer);
        _layout->addItem(_spacer);
    }

    if(_type == SLTable::Type_KnockOut) {
        for (int i = 0 ; i < _nbr ; i++) {
            _knkOutVect[i]->setText(_knkOutVect[i]->qualifie(_rulAwayGls));
        }
    }
}

void SLTable::setBckGrdColorTeams(int position) {
    QString color;
    for(int i = 0 ; i < _bckGrdColor.size(); i++) {
        if(_bckGrdColor[i][0] <= position && _bckGrdColor[i][1] >= position) {
            color = QString::number(_bckGrdColor[i][2], 16).toUpper();

            //To have a 6-char string for the color
            int sze = color.size();
            for(int i = sze; i < 6 ; i++) {
                color = "0" + color;
            }

            _rndRobVect[position-1]->setStyleSheet("background-color: #"+color+";");
        }
    }
}

/// @brief Sort the Teams (Round-Robin only)
/// @remarks recursive function
void SLTable::sortTeams(int idBeg, int idEnd, int i_crit) {
    if(i_crit < _criterions.size()) {
        int crit = _criterions[i_crit];
        int sze = 0;
        std::vector<Team*>::iterator beg = _rndRobVect.begin() + idBeg;
        std::vector<Team*>::iterator end = _rndRobVect.begin() + (idEnd + 1);

        if(crit == CRIT_PTS_G)
            sort(beg, end, Pts_g());
        else if(crit == CRIT_STD)
            sort(beg, end, Stdd());
        else if(crit == CRIT_MAN)
            sort(beg, end, Man());
        else {
            std::vector<int> idTab;

            for(int i = idBeg ; i <= idEnd ; i++) {
                idTab.push_back(_rndRobVect[i]->getId());
            }

            for(int i = idBeg ; i <= idEnd ; i++) {
                _rndRobVect[i]->maj_sort(idTab);
            }

            if(crit == CRIT_SPE_SSBMEXT || crit == CRIT_SPE_SSBMEXT_REC)
                sort(beg, end, Spe_ssBMExt());
            else if(crit == CRIT_SPE_ACBMEXT || crit == CRIT_SPE_ACBMEXT_REC)
                sort(beg, end, Spe_acBMExt());
            else if(crit == CRIT_DIFF_P)
                sort(beg, end, Diff_p());

            idTab.clear();
        }

        int bgn = idBeg;
        for (int i = idBeg ; i <= idEnd ; i++) {
            sze++;

            if(i != idEnd) {
                if(_rndRobVect[i]->estEgalA(_rndRobVect[i+1], crit)) {
                } else {
                    if (sze != 1) {
                        if(crit == CRIT_SPE_SSBMEXT_REC || crit == CRIT_SPE_ACBMEXT_REC)
                            sortTeams(bgn, i, i_crit);        //REC (1)
                        else
                            sortTeams(bgn, i, i_crit + 1);    //REC (2)
                    }

                    bgn = i + 1;
                    sze = 0;
                }
            } else {
                if (sze != 1) {
                    if(sze < (idEnd - idBeg + 1) && (crit == CRIT_SPE_SSBMEXT_REC || crit == CRIT_SPE_ACBMEXT_REC))
                        sortTeams(bgn, i, i_crit);            //REC (3)
                    else
                        sortTeams(bgn, i, i_crit + 1);        //REC (4)
                }
            }
        } // For
    } // if(i_crit < _criterions.size())
}

