/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SLMatchWiz.h"

SLMatchWiz::SLMatchWiz(int act, QWidget *parent) : QWizard(parent), _nbr(0), _act(act) {
    setModal(true);

    _lyt = new QVBoxLayout(this);
    _lytEdit = new QVBoxLayout(this);
    _name = new QLineEdit(this);
      _name->hide();
    _nbrMt = new QSpinBox(this);
      _nbrMt->hide();

    if(_act == MATCHS_NEW) {
        setPage(Page_IntroMt, nvIntroPage());
        setPage(Page_Infos, nvInfosPage());
        setPage(Page_Match, nvMatchPage());

        setStartId(Page_IntroMt);

        setWindowTitle(tr("Journée : création"));

        QFrame *hd = new QFrame(this);
            _uiHeader.setupUi(hd);
            _lyt->addWidget(hd);
            QObject::connect(_uiHeader.loadMatchDay, SIGNAL(clicked()), this, SLOT(loadMatchDayEmit()));
            QObject::connect(_uiHeader.swapTeams, SIGNAL(clicked()), this, SLOT(swapTeams()));
    } else if(_act == MATCHS_EDIT) {
        setPage(Page_EditMt, edMatchsPage());

        setStartId(Page_EditMt);

        setWindowTitle(tr("Journée : édition"));

        _nbrMt->hide();
    }
}

QWizardPage* SLMatchWiz::nvIntroPage() {
    QWizardPage *page = new QWizardPage(this);
        page->setTitle(tr("Introduction"));
    QVBoxLayout *layout = new QVBoxLayout(page);
        page->setLayout(layout);

    QLabel *brief = new QLabel(tr("Cet assistant va vous permettre d'ajouter une journée avec des matchs."), this);
        brief->setWordWrap(true);
        layout->addWidget(brief);

    return page;
}

QWizardPage *SLMatchWiz::nvInfosPage() {
    QWizardPage *page = new QWizardPage(this);
        page->setTitle(tr("Informations"));
        page->setSubTitle(tr("Nom & nombre matchs"));
    QVBoxLayout *layout = new QVBoxLayout(page);
        page->setLayout(layout);
    
    QFrame *titleMatchDay = new QFrame(this);
        layout->addWidget(titleMatchDay);
    QHBoxLayout *hbox0 = new QHBoxLayout(titleMatchDay);
        titleMatchDay->setLayout(hbox0);
    QLabel *label0 = new QLabel(tr("En-tête : "), this);
        label0->setMaximumWidth(120);
        hbox0->addWidget(label0);
    //_name
        hbox0->addWidget(_name);
    
    QFrame *nbrMatch = new QFrame(this); 
        layout->addWidget(nbrMatch); 
    QHBoxLayout *hbox1 = new QHBoxLayout(nbrMatch);
        nbrMatch->setLayout(hbox1);
    QLabel *label1 = new QLabel(tr("Nombre de matchs : "), this);
        label1->setMaximumWidth(120);
        hbox1->addWidget(label1);
    //_nbrMt
        _nbrMt->setMinimum(0);
        _nbrMt->setMaximumWidth(50);
        QObject::connect(_nbrMt, SIGNAL(valueChanged(int)), this, SLOT(setNbrMt(int)));
        hbox1->addWidget(_nbrMt);

    return page;
}

QWizardPage *SLMatchWiz::nvMatchPage() {
    QWizardPage *page = new QWizardPage(this);
        page->setTitle(tr("Matchs"));
        page->setSubTitle(tr("Propriétés des matchs"));
    //_lyt
        page->setLayout(_lyt);

    return page;
}

QWizardPage *SLMatchWiz::edMatchsPage() {
    QWizardPage *page = new QWizardPage(this);
        page->setTitle(tr("Edition"));
        page->setSubTitle(tr("En-tête et ordre des matchs"));
    //_lytEditit 
        page->setLayout(_lytEdit);
 

    QFrame *titleMatchDay = new QFrame(page);
        _lytEdit->addWidget(titleMatchDay);
    QHBoxLayout *hbox0 = new QHBoxLayout(page); 
        titleMatchDay->setLayout(hbox0);
    QLabel *label0 = new QLabel(tr("En-tête : "), page);
        label0->setMaximumWidth(120);
        hbox0->addWidget(label0);
    //_name
        hbox0->addWidget(_name);

    return page;
}

void SLMatchWiz::setNwMatchPage(std::vector<QString> const &matchDayTitles) {
    //matchDayTitles
    _uiHeader.matchDayTitles->clear();
    
    //First Item : No load MatchDay
    _uiHeader.matchDayTitles->addItem(tr("_Ne rien charger_"));

    for (int i = 0 ; i < matchDayTitles.size() ; i++) {
        _uiHeader.matchDayTitles->addItem(matchDayTitles[i]);
    }
    
    _uiHeader.matchDayTitles->setCurrentIndex(0);
}

void SLMatchWiz::setEdPage(QString const &title, std::vector<QString> const &nomLocal, std::vector<QString> const &nomVisiteur) {
    //title
    _name->setText(title);

    //hstName + vstName
    for(int i = 0 ; i < nomLocal.size() ; i++) {
        QFrame *match = new QFrame(this);
        _vectUiEditMt.push_back(Ui::SLMatchWizEditItem());
        _vectUiEditMt[i].setupUi(match);
            _vectUiEditMt[i].hstName->setText(nomLocal[i]);
            _vectUiEditMt[i].vstName->setText(nomVisiteur[i]);
            _vectUiEditMt[i].order->setValue(i);
        _vectMatch.push_back(match);
        _lytEdit->addWidget(match);
    }
}

int SLMatchWiz::nextId() const {
    switch (currentId()) {
        case Page_IntroMt:
            return Page_Infos;
        break;
        case Page_Infos:
            return Page_Match;
        break;
        case Page_Match: case Page_EditMt: default:
            return -1;
        break;
    }
}

void SLMatchWiz::setNbrMt(int nvNbrMt) {
    bool valueChangedPlus = (_nbr < nvNbrMt);

    if(valueChangedPlus) {
        for (int i = _nbr; i < nvNbrMt; i++) {
            QFrame *matchItem = new QFrame(this);
            _vectUiMatch.push_back(Ui::SLMatchWizItem());
            _vectUiMatch[i].setupUi(matchItem);
            _vectUiMatch[i].id->setText(QString::number(i+1));
            _vectMatch.push_back(matchItem);
            _lyt->addWidget(matchItem);
        }
    } else {
        for (int i = _nbr; i > nvNbrMt; i--) {
            _lyt->removeWidget(_vectMatch[i-1]);
            _vectMatch[i-1]->hide();

            delete _vectMatch[i-1];
            _vectMatch.pop_back();

            _vectUiMatch.pop_back();
        }
    }

    _nbr = nvNbrMt;
}

void SLMatchWiz::loadMatchDayEmit() {
    int indexMD = _uiHeader.matchDayTitles->currentIndex();
    
    if (indexMD != 0) {
        int reponse = QMessageBox::information(this, tr("Charger Journée"), tr("Voulez-vous charger cette journée ?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
        
        switch(reponse) {
            case QMessageBox::Yes:
                emit loadMD((indexMD - 1));
            break;
            default:
            break;
        }
    }
}

void SLMatchWiz::loadMatchDay(std::vector<QString> const &hstNames, std::vector<QString> const &vstNames) {
    if(_act == MATCHS_NEW) {
        _nbrMt->setValue(hstNames.size());

        for(int i = 0 ; i < hstNames.size() ; i++) {
            _vectUiMatch[i].hstName->setText(hstNames[i]);
            _vectUiMatch[i].vstName->setText(vstNames[i]);
        }
    }
}

void SLMatchWiz::swapTeams() {
    QString swapTmp;
    
    for (int i = 0 ; i < _vectUiMatch.size() ; i++) {
        swapTmp = _vectUiMatch[i].hstName->text();
        _vectUiMatch[i].hstName->setText(_vectUiMatch[i].vstName->text());
        _vectUiMatch[i].vstName->setText(swapTmp); 
    }
}

int SLMatchWiz::getAction() const {
    return _act;
}

QString SLMatchWiz::getMatchDayName() const {
    return _name->text();
}

int SLMatchWiz::getNbrMt() const {
    return _nbr;
}

std::vector<int> SLMatchWiz::getOrdre() const {
    std::vector<int> vect;

    for(int i = 0 ; i < _vectUiEditMt.size() ; i++) {
        vect.push_back(_vectUiEditMt[i].order->value());
    }

    return vect;
}

std::vector<bool> SLMatchWiz::getNoDraws() const {
    std::vector<bool> vect;
    
    for(int i = 0 ; i < _vectUiMatch.size() ; i++) {
        vect.push_back(_vectUiMatch[i].extraTime->isChecked());
    }
     
    return vect;
}

std::vector< std::vector<QString> > SLMatchWiz::getTeamsNames() const {
    std::vector< std::vector<QString> > matr;
    std::vector<QString> vect;
    
    for(int i = 0 ; i < _vectUiMatch.size() ; i++) {
        vect.clear();
        
        vect.push_back(_vectUiMatch[i].hstName->text());
        vect.push_back(_vectUiMatch[i].vstName->text());
        
        matr.push_back(vect);
    }
    
    return matr;
}
