/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Results.h"

Results::Results(QWidget *parent) : QFrame(parent) {
    _ClsDft = -1;
    _JrDft = -1;
    _JrEna = -1;
    _ClsEna = -1;

    showAnotherMD = true;

    setFrameStyle(QFrame::Panel | QFrame::Plain);
    setLineWidth(1);
    setStyleSheet("background-color: #5A5A5A");

    layout = new QVBoxLayout(this);
        layout->setSpacing(1);
        layout->setContentsMargins(0, 0, 0, 1);
        layout->setSizeConstraint(QLayout::SetMinimumSize);
        setLayout(layout);

    _head = new QFrame(this);
        ui.setupUi(_head);
        layout->addWidget(_head);

    space = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
        layout->addItem(space);

    ui.NomJournee->setText("___");

    QObject::connect(ui.BoutonBas, SIGNAL(clicked()), this, SLOT(downBut()));
    QObject::connect(ui.BoutonHaut, SIGNAL(clicked()), this, SLOT(upBut()));
    QObject::connect(ui.BoutonGauche, SIGNAL(clicked()), this, SLOT(leftBut()));
    QObject::connect(ui.BoutonDroite, SIGNAL(clicked()), this, SLOT(rightBut()));

    majAffNavig();
}

Results::~Results() {
    clear();
}

void Results::clear() {
   showAnotherMD = false;

    for(int i = 0 ; i < tabl.size() ; i++)
        for(int j = 0 ; j < tabl[i].size() ; j++)
            delete tabl[i][j];
    tabl.clear();

    _nbr.clear();

    setMinimumHeight(0);

    _ClsDft = -1;
    _JrDft = -1;
    _JrEna = -1;
    _ClsEna = -1;

    ui.NomJournee->setText("___");
    majAffNavig();

    showAnotherMD = true;
}

void Results::ajGroupe() {
    if(_nbr.size() > 0) {
        if (_nbr[_ClsEna] > 0) {
            tabl[_ClsEna][_JrEna]->hide();
            ui.NomJournee->setText("");
        }
    }

    _nbr.push_back(0);
    tabl.push_back(std::vector<MatchDay*>());
    _ClsEna = _nbr.size()-1;
    _JrEna = -1;
    _ClsDft = -1;
    _JrDft = -1;

    majAffNavig();
}

void Results::delLastSlTable() {
   //on retire le dernier vector de MatchDay
   tabl.pop_back();
   _nbr.pop_back();

   //On affiche le groupe d'avant, le premier MatchDay
   _ClsEna = _nbr.size()-1;
   _JrEna = 0;

   changeMatchDay(_ClsEna, _JrEna);

   majAffNavig();
}

void Results::setFstJr(int const clsDft, int const jrDft) {
    _ClsDft = clsDft;
    _JrDft = jrDft;

    if(_ClsDft >= 0 && _ClsDft < tabl.size()) {
        if(_JrDft >= 0 && _JrDft < tabl[_ClsDft].size()) {
            changeMatchDay(_ClsDft, _JrDft);
        }
        emit changeSLTable(_ClsDft);
    }
}

void Results::addItem(MatchDay *item, int const grLink) {
    if(_JrEna >= 0 && _ClsEna >= 0 && _ClsDft != _ClsEna && _JrDft != _JrEna) {
        tabl[_ClsEna][_JrEna]->hide();
    }

    if(grLink >= 0 && grLink < _nbr.size())
        _ClsEna = grLink;

    _nbr[_ClsEna]++;
    _JrEna = _nbr[_ClsEna]-1;

    tabl[_ClsEna].push_back(item);
    if((_ClsDft == -1 && _JrDft == -1) || (_ClsDft == _ClsEna && _JrDft == _JrEna)) {
        item->show();

        ui.NomJournee->setText(item->title());
    } else {
        item->hide();

        _ClsEna = _ClsDft; _JrEna = _JrDft;
    }
    
    layout->removeItem(space);
    layout->addWidget(item);
    layout->addItem(space);

    QObject::connect(item, SIGNAL(contentsChanged()), this, SIGNAL(contentsChanged()));
    QObject::connect(item, SIGNAL(loadMD(int)), this, SLOT(loadMatchDay(int)));
    QObject::connect(item, SIGNAL(destroyed(QObject*)), this, SLOT(destroyMatchDay(QObject*)));

    majAffNavig();
}

void Results::addItem(Match *item, int const grLink, int const journee) {
    int clsLk = -1;
    int journ = -1;

    if(grLink >= 0 && grLink < _nbr.size())
        clsLk = grLink;
    else
        clsLk = _ClsEna;

    if ((journee >= 0) && (journee < _nbr[clsLk]))
        journ = journee;
    else
        journ = _nbr[clsLk]-1;

    tabl[clsLk][journ]->addItem(item);
}


/// @brief delete the pointor in tabl and show another MatchDay (obj is auto-destroyed)
void Results::destroyMatchDay(QObject *obj) {
   if(showAnotherMD && &(*(tabl[_ClsEna][_JrEna])) == &(*obj)) {
      tabl[_ClsEna][_JrEna]->hide();

      //Derniere journee :
      if(_JrEna = _nbr[_ClsEna] - 1) {
         tabl[_ClsEna].pop_back();
         _nbr[_ClsEna]--;
         //On affiche la journee précédente (si elle existe)
         _JrEna -= 1;
         if(_JrEna >= 0)
            changeMatchDay(_ClsEna, _JrEna);
         else
            _JrEna = 0;
      }
   }

   majAffNavig();
}

QString Results::getXml() const {
    QString text = "";

    text += "\t\t<Results ";
    text += QString("clsEna=\"%1\" jrEna=\"%2\" >\n")
            .arg(_ClsEna).arg(_JrEna);
    for (int i = 0 ; i < _nbr.size() ; i++) {
        for (int j = 0 ; j < _nbr[i] ; j++) {
            text += tabl[i][j]->getXml(i,j); //MatchDay
        }
    }
    text += "\t\t</Results>\n";

    return text;
}

void Results::upBut() {
    if (_JrEna > 0) {
        changeMatchDay(_ClsEna, _JrEna - 1);

        majAffNavig();
    }
}

void Results::downBut() {
    if (_JrEna < _nbr[_ClsEna] - 1) {
        changeMatchDay(_ClsEna, _JrEna + 1);

        majAffNavig();
    }
}

void Results::leftBut() {
    if (_ClsEna > 0) {
        if(_nbr[_ClsEna - 1] - 1 >= _JrEna)
            changeMatchDay(_ClsEna - 1, _JrEna);
        else
            changeMatchDay(_ClsEna - 1, _nbr[_ClsEna - 1] - 1);

        emit changeSLTable(_ClsEna);

        majAffNavig();
    }
}

void Results::rightBut() {
    if (_ClsEna < _nbr.size() - 1) {
        if(_nbr[_ClsEna + 1] - 1 >= _JrEna)
            changeMatchDay(_ClsEna + 1, _JrEna);
        else
            changeMatchDay(_ClsEna + 1, _nbr[_ClsEna + 1] - 1);

        emit changeSLTable(_ClsEna);

        majAffNavig();
    }
}

void Results::changeMatchDay(int const nvclsLinkActive, int const nvLiveGrActive) {
    _ClsDft = -1; _JrDft = -1;

    if(_ClsEna >= 0 && _JrEna >= 0)
        tabl[_ClsEna][_JrEna]->hide();

    _ClsEna = nvclsLinkActive;

    if(nvLiveGrActive >= 0) {
        _JrEna = nvLiveGrActive;

        ui.NomJournee->setText(tabl[_ClsEna][_JrEna]->title());

        tabl[_ClsEna][_JrEna]->show();
    } else {
        _JrEna = -1;
    }

    emit contentsChanged();
}


void Results::execMatchDayWizard(int action, SLTable *slTablePtr) {
    if(action == MATCHS_NEW) {
        MatchDay *newMatchDay = new MatchDay("", slTablePtr);
        _ClsDft = -1; _JrDft = -1;
        addItem(newMatchDay, _ClsEna);
        newMatchDay->execWizard(action, getCurrentSLTblMDTitles(_ClsEna));
    } else if(action == MATCHS_EDIT) {
        tabl[_ClsEna][_JrEna]->execWizard(action);
    }
}

std::vector<QString> Results::getCurrentSLTblMDTitles(int const SLTableEna) const {
    std::vector<QString> vect;
    
    for(int i = 0 ; i < tabl[SLTableEna].size() ; i++) {
        vect.push_back(tabl[SLTableEna][i]->title());
    }
    
    return vect;
}

void Results::loadMatchDay(int index) {
    if(index >= 0 && index < tabl[_ClsEna].size())
        tabl[_ClsEna][_JrEna]->loadMatchDay(tabl[_ClsEna][index]);
    else
        QMessageBox::warning(this, tr("Erreur Results::loadMatchDay()"), tr("Erreur lors du chargement de la journée mod?le"));
}

void Results::majAffNavig() {
    QString ena = "QPushButton {\n"
                  "font: 75 8pt \"MS Shell Dlg 2\";\n"
                  "color : #000000;\n"
                  "background-color: #646464;\n"
                  "}\n"
                  "QPushButton:hover {\n"
                  "font: 100 8pt \"MS Shell Dlg 2\";\n"
                  "color : #FFFFFF;\n"
                  "background-color: #4B4B4B;\n"
                  "}\n"
                  "QPushButton:pressed {\n"
                  "color : #3CFFFF;\n"
                  "background-color: #0A4B4B;\n"
                  "}";
    QString dis = "QPushButton {\n"
                  "font: 75 8pt \"MS Shell Dlg 2\";\n"
                  "color : #000000;\n"
                  "background-color: #000000;\n"
                  "}";

    ui.BoutonBas->setStyleSheet(dis);
    ui.BoutonBas->setEnabled(false);
    ui.BoutonHaut->setStyleSheet(dis);
    ui.BoutonHaut->setEnabled(false);
    ui.BoutonGauche->setStyleSheet(dis);
    ui.BoutonGauche->setEnabled(false);
    ui.BoutonDroite->setStyleSheet(dis);
    ui.BoutonDroite->setEnabled(false);

    if(_ClsDft == -1 && _JrDft == -1) {
        if(_nbr.size() > 0) {
            if(_JrEna < _nbr[_ClsEna] - 1) {
                ui.BoutonBas->setStyleSheet(ena);
                ui.BoutonBas->setEnabled(true);
            }
        }
        if(_JrEna > 0) {
            ui.BoutonHaut->setStyleSheet(ena);
            ui.BoutonHaut->setEnabled(true);
        }
        if(_ClsEna > 0) {
            ui.BoutonGauche->setStyleSheet(ena);
            ui.BoutonGauche->setEnabled(true);
        }
        if(_ClsEna < _nbr.size() - 1) {
            ui.BoutonDroite->setStyleSheet(ena);
            ui.BoutonDroite->setEnabled(true);
        }
    } else {
        if(_nbr.size() > 0) {
            if(_JrDft < _nbr[_ClsDft] - 1) {
                ui.BoutonBas->setStyleSheet(ena);
                ui.BoutonBas->setEnabled(true);
            }
        }
        if(_JrDft > 0) {
            ui.BoutonHaut->setStyleSheet(ena);
            ui.BoutonHaut->setEnabled(true);
        }
        if(_ClsDft > 0) {
            ui.BoutonGauche->setStyleSheet(ena);
            ui.BoutonGauche->setEnabled(true);
        }
        if(_ClsDft < _nbr.size() - 1) {
            ui.BoutonDroite->setStyleSheet(ena);
            ui.BoutonDroite->setEnabled(true);
        }
    }
}
