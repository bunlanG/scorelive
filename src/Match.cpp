/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Match.h"
#include "SLTable.h"

std::vector<Match::StatusItem> Match::_StatusProgress = Match::setupStatusProgress();

/* Constructor */
Match::Match(std::vector<QString> const &teamsName, std::vector< std::vector<int> > const &score, int const status, bool const noDraw, bool const inPenSh, SLTable *slTableLink, QWidget *parent) : QFrame(parent), _teamsName(teamsName), _score(score), _statusId(status), _multiCorr(1), _ordre(0), _extraTime(noDraw), _inPenSh(inPenSh) , _waitAns(false) {
    ui.setupUi(this);

    QObject::connect(ui.hstName, SIGNAL(clicked()), this, SLOT(hstGoal()));
    QObject::connect(ui.vstName, SIGNAL(clicked()), this, SLOT(vstGoal()));
    QObject::connect(ui.Status, SIGNAL(clicked()), this, SLOT(statusChanged()));
    QObject::connect(ui.Correction, SIGNAL(clicked()), this, SLOT(correction()));
    
    ui.hstName->setText(_teamsName[0]);
    ui.vstName->setText(_teamsName[1]);
    
    QObject::connect(this, SIGNAL(matchUpdated(QString,QString,std::map<QString,int>,std::map<QString,int>)), slTableLink, SLOT(update(QString,QString,std::map<QString,int>,std::map<QString,int>)));
    QObject::connect(this, SIGNAL(matchMayBeFinish(Match*,QString,QString)), slTableLink, SLOT(isFini(Match*,QString,QString)));
    
    setBckGrdColor();
    setScoreColor();
}

QString Match::getXml() const {
    QString text = "";
    int intNoDraw = 0;
    int intInPenSh = 0;

    if(_extraTime) {
        intNoDraw = 1;
        if(_inPenSh)
            intInPenSh = 1;
    }

    text += QString("\t\t\t\t<Match teams=\"%1\" scr=\"%2\" sts=\"%3\" ")
            .arg(SLExpressions::strVectToExp(_teamsName))
            .arg(SLExpressions::intMatrToExp(_score))
            .arg(_statusId);
    if(_extraTime) {
        text += QString("noDraw=\"%1\" inPenSh=\"%2\" ")
                .arg(intNoDraw)
                .arg(intInPenSh);
    }
    text += "/>\n";

    return text;
}

/*  Pour les étourdis... ^^ Slot relié au bouton "Correction" permettant
 *  d'enlever des points si active
 */
void Match::correction() {
    _multiCorr *= -1;
    setBckGrdColor();
}

void Match::hstGoal() {
    //Vous avez deja vu un match avec un score négatif ??? ^^
    if(_score[0][0] == 0 && _multiCorr == -1 && !_inPenSh) {
        return;
    } else if(_extraTime) {
        if(_score[2][0] == 0 && _multiCorr == -1 && _inPenSh) {
            return;
        }
    }
    emit contentsChanged();

    if(_inPenSh)
        _score[2][0] += _multiCorr;
    else
        _score[0][0] += _multiCorr;

    setScoreColor();

    //Update the linked SLTable...
    //La differenciation du _multiCorr vient que un but enlevé a une equipe ou un but ajoute a l'autre,
    //c'est quasiment la meme chose pour le match : ca avantage l'autre equipe... o.O"
    if (_multiCorr == -1)
        updateSLTable(Match::Reason_VstGoal);
    else
        updateSLTable(Match::Reason_HstGoal);
}

void Match::vstGoal() {
    if(_score[0][1] == 0 && _multiCorr == -1 && !_inPenSh) {
        return;
    } else if(_extraTime) {
        if(_score[2][1] == 0 && _multiCorr == -1 && _inPenSh) {
            return;
        }
    }

    emit contentsChanged();

    if(_inPenSh)
        _score[2][1] += _multiCorr;
    else
        _score[0][1] += _multiCorr;

    setScoreColor();

    //Envoi le but au Classement...
    //La differenciation du _multiCorr vient que un but enlevé a une equipe ou un but ajoute a l'autre,
    //c'est quasiment la meme chose pour le match : ca avantage l'autre equipe... o.O"
    if (_multiCorr == -1)
        updateSLTable(Match::Reason_HstGoal);
    else
        updateSLTable(Match::Reason_VstGoal);
}

void Match::statusChanged() {
    emit contentsChanged();
    
    if(_multiCorr == 1) {
        if(_statusId < _StatusProgress.size() - 1) {
            _statusId++;
        }
         
        if (_StatusProgress[_statusId - 1].family == Match::Status_NotBegun)
            updateSLTable(Match::Reason_StsNoBg2Run);
        if (_StatusProgress[_statusId].family == Match::Status_Finished)
            updateSLTable(Match::Reason_StsRun2End);
        
        //Match can be finish here
        if (_StatusProgress[_statusId].infosItem > 0) {
            if (!_extraTime) {
                //Match is finished
                _statusId = _StatusProgress.size() - 1;
                statusChanged();
            } else {
                //We wait an answer to continue the match, but always run waiting an answer
                _waitAns = true;
                emit matchMayBeFinish(this, _teamsName[0], _teamsName[1]);
                if (_statusId == 10)
                    _inPenSh = true;
            }
        }
    } else if (_multiCorr == -1) {
        if (_statusId > 0) {
            _statusId--;
        }
        
        if (_StatusProgress[_statusId].family == Match::Status_NotBegun)
            updateSLTable(Match::Reason_StsRun2NoBg);
        if (_StatusProgress[_statusId + 1].family == Match::Status_Finished) {
            updateSLTable(Match::Reason_StsEnd2Run);
            _statusId = 5;
        }

        if (_statusId < 10)
            _inPenSh = false;
    }
    
    setBckGrdColor();
    setScoreColor();
}

void Match::mayBeFinish(bool const isFinish) {
    if (_waitAns) {
        _waitAns = false;
        if (isFinish) {
            //Match is also finished
            _statusId = _StatusProgress.size() - 1;
            statusChanged();
        }
    }
}

void Match::updateSLTable(int reason) {
    std::map<QString,int> changeMapHst;
    std::map<QString,int> changeMapVst;

    switch (reason) {
        case Match::Reason_HstGoal:
            if (_score[0][0] == _score[0][1]) {
                changeMapHst["Loses"] = -1;
                changeMapHst["Draws"] = 1;
                
                changeMapVst["Wins"] = -1;
                changeMapVst["Draws"] = 1;
            } else if (_score[0][0] == _score[0][1] + 1) {
                changeMapHst["Draws"] = -1;
                changeMapHst["Wins"] = 1;
                
                changeMapVst["Draws"] = -1;
                changeMapVst["Loses"] = 1;
            }

            if (_multiCorr == 1) {
                if(_inPenSh)
                    changeMapHst["Goals_SO"] = 1;
                else
                    changeMapHst["Goals"] = 1;
            } else {
                if(_inPenSh)
                    changeMapVst["Goals_SO"] = -1;
                else
                    changeMapVst["Goals"] = -1; 
            }
        break;

        case Match::Reason_VstGoal:
            if (_score[0][0] == _score[0][1]) {
                changeMapHst["Wins"] = -1;
                changeMapHst["Draws"] = 1;
                
                changeMapVst["Loses"] = -1;
                changeMapVst["Draws"] = 1;
            } else if (_score[0][0] + 1 == _score[0][1]) {
                changeMapHst["Draws"] = -1;
                changeMapHst["Loses"] = 1;
                
                changeMapVst["Draws"] = -1;
                changeMapVst["Wins"] = 1;
            }

            if (_multiCorr == 1) {
                if(_inPenSh)
                    changeMapVst["Goals_SO"] = 1;
                else
                    changeMapVst["Goals"] = 1;
            } else {
                if(_inPenSh)
                    changeMapHst["Goals_SO"] = -1;
                else
                    changeMapHst["Goals"] = -1;
            }
        break;

        case Match::Reason_StsRun2NoBg:
            if (_score[0][0] > _score[0][1]) {
                changeMapHst["Wins"] = -1;
                
                changeMapVst["Loses"] = -1;
            } else if (_score[0][0] < _score[0][1]) { 
                changeMapHst["Loses"] = -1;
                
                changeMapVst["Wins"] = -1;
            } else {
                changeMapHst["Draws"] = -1;
                
                changeMapVst["Draws"] = -1;
            }

            changeMapHst["Goals"] = -1 * _score[0][0];
            changeMapVst["Goals"] = -1 * _score[0][1];
        break;

        case Match::Reason_StsNoBg2Run:
            if (_score[0][0] > _score[0][1]) {
                changeMapHst["Wins"] = 1;
                
                changeMapVst["Loses"] = 1;
            } else if (_score[0][0] < _score[0][1]) {
                changeMapHst["Loses"] = 1;
                
                changeMapVst["Wins"] = 1;
            } else {
                changeMapHst["Draws"] = 1;
                
                changeMapVst["Draws"] = 1;
            }

            changeMapHst["Goals"] = _score[0][0];
            changeMapVst["Goals"] = _score[0][1];
        break;
        
        case Match::Reason_StsRun2End:
        break;
        
        case Match::Reason_StsEnd2Run:
        break;

        default:
            return;
        break;
    }
    
    emit matchUpdated(_teamsName[0], _teamsName[1], changeMapHst, changeMapVst);
}

//  Permet de modifier le style en fonction du statut
void Match::setBckGrdColor() {
    //Set the Correction Button
    if(_multiCorr == 1) {
        ui.Correction->setStyleSheet("QPushButton {\n"
                                     "color: #DDDDDD;\n"
                                     "background-color: #1E5A1E;\n"
                                     "}\n");
        if(_statusId == _StatusProgress.size() - 1)
            ui.Status->setEnabled(false);
        else
            ui.Status->setEnabled(true);
            
    } else if(_multiCorr == -1) {
        ui.Correction->setStyleSheet("QPushButton {\n"
                                     "color: #DDDDDD;\n"
                                     "background-color: #5A1E1E;\n"
                                     "}\n");
        if(_statusId == 0)
            ui.Status->setEnabled(false);
        else
            ui.Status->setEnabled(true); 
    }
    
    //Set the status Button
    ui.Status->setText(_StatusProgress[_statusId].name);

    switch (_StatusProgress[_statusId].family) {
        case Match::Status_Aborted:
            setStyleSheet("background-color: #0F3152");
            ui.Status->setStyleSheet("QPushButton {\n"
                                     "color: #64C2F2;\n"
                                     "background-color: #031B25;\n"
                                     "}\n");
            ui.hstName->setEnabled(false);
            ui.vstName->setEnabled(false);
        break;

        case Match::Status_NotBegun:
            setStyleSheet("background-color: #282828");
            ui.Status->setStyleSheet("QPushButton {\n"
                                     "color: #AAAAAA;\n"
                                     "background-color: #141414;\n"
                                     "}\n");
            ui.hstName->setEnabled(false);
            ui.vstName->setEnabled(false);
        break;

        case Match::Status_Running:
            setStyleSheet("background-color: #0F420F");
            ui.Status->setStyleSheet("QPushButton {\n"
                                     "color: #64F064;\n"
                                     "background-color: #032503;\n"
                                     "}\n");
            ui.hstName->setEnabled(true);
            ui.vstName->setEnabled(true);
        break;

        case Match::Status_Paused:
            setStyleSheet("background-color: #423C0F");
            ui.Status->setStyleSheet("QPushButton {\n"
                                     "color: #F0DF64;\n"
                                     "background-color: #252103;\n"
                                     "}\n");
            ui.hstName->setEnabled(false);
            ui.vstName->setEnabled(false);
        break;

        case Match::Status_Finished:
            setStyleSheet("background-color: #420F0F");
            ui.Status->setStyleSheet("QPushButton {\n"
                                     "color: #F06464;\n"
                                     "background-color: #250303;\n"
                                     "}\n");
            ui.hstName->setEnabled(false);
            ui.vstName->setEnabled(false);
        break;

        default:
        break;
    }
}

void Match::setScoreColor() {
    QString win =   "QPushButton {\n"
                    "color: #51D751;\n"
                    "}\n";
    QString draw =  "QPushButton {\n"
                    "color: #5173D7;\n"
                    "}\n";
    QString lose =  "QPushButton {\n"
                    "color: #D75151;\n"
                    "}\n";
    QString nc =    "QPushButton {\n"
                    "color: #646464;\n"
                    "}\n";
    QFont font1;
        font1.setFamily(QString::fromUtf8("Nokia Pure Text"));
        font1.setPointSize(16);
        font1.setBold(true);
        font1.setItalic(true);
        font1.setWeight(75);
    QFont font2;
        font2.setFamily(QString::fromUtf8("Nokia Pure Text"));
        font2.setPointSize(11);

    if (!_inPenSh) {
        ui.scr01->setFont(font1);
        if(_StatusProgress[_statusId].infosItem  < 0)
            ui.scr01->setText("-");
        else
            ui.scr01->setText(QString("%1-%2")
                             .arg(_score[0][0]).arg(_score[0][1]));
        ui.scr02->hide();

        if (_StatusProgress[_statusId].infosItem  < 0) {
            ui.hstName->setStyleSheet(nc);
            ui.vstName->setStyleSheet(nc);
        } else if (_score[0][0] < _score[0][1]) {
            ui.hstName->setStyleSheet(lose);
             ui.vstName->setStyleSheet(win);
        } else if (_score[0][0] > _score[0][1]) {
            ui.hstName->setStyleSheet(win);
            ui.vstName->setStyleSheet(lose);
        } else {
            ui.hstName->setStyleSheet(draw);
            ui.vstName->setStyleSheet(draw);
        }
    } else {
        ui.scr01->setFont(font2);
        ui.scr01->setText(QString("%1-%2")
                          .arg(_score[2][0]).arg(_score[2][1]));
        ui.scr02->show();
        ui.scr02->setText(QString("(%1-%2 ap)")
                         .arg(_score[0][0]).arg(_score[0][1]));

        if (_score[2][0] < _score[2][1]) {
            ui.hstName->setStyleSheet(lose);
            ui.vstName->setStyleSheet(win);
        } else if (_score[2][0] > _score[2][1]) {
            ui.hstName->setStyleSheet(win);
            ui.vstName->setStyleSheet(lose);
        } else {
            ui.hstName->setStyleSheet(draw);
            ui.vstName->setStyleSheet(draw);
        }
    }
}

void Match::setnvOrdre(int nvOrdre) {
    _ordre = nvOrdre;
}

bool Match::lessThan(Match const& b) const {
    if (_ordre < b._ordre)
        return true;
    else if (_ordre == b._ordre)
        return _teamsName[0] < b._teamsName[0];

    return false;
}
