/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Versus.h"

Versus::Versus(std::vector<QString> const &teamsName, std::vector<int> const &diffs, QWidget *parent) : QLabel(parent), _teamsName(teamsName), _diffs(diffs) {
}



QString Versus::getXml() const {
    QString text = "";

    text += QString("\t\t\t\t<Versus teams=\"%1\" diffs=\"%2\" />\n")
            .arg(SLExpressions::strVectToExp(_teamsName))
            .arg(SLExpressions::intVectToExp(_diffs));
   
    return text;
}

void Versus::update(bool const team1IsHost, std::map<QString,int> changeMapTeam1, std::map<QString,int> changeMapTeam2) {
    int chgDiffPenSh = 0;
    int chgDiff = 0;
    int chgDiffAway = 0;
    
    if (changeMapTeam1.find("Goals") != changeMapTeam1.end()) {
        chgDiff += changeMapTeam1["Goals"];
        if (!team1IsHost)
            chgDiffAway += changeMapTeam1["Goals"];
    }
    if (changeMapTeam2.find("Goals") != changeMapTeam2.end()) {
        chgDiff -= changeMapTeam2["Goals"];
        if (team1IsHost)
            chgDiffAway -= changeMapTeam2["Goals"];
    }
    if (changeMapTeam1.find("Goals_SO") != changeMapTeam1.end())
        chgDiffPenSh += changeMapTeam1["Goals_SO"];
    if (changeMapTeam2.find("Goals_SO") != changeMapTeam2.end())
        chgDiffPenSh -= changeMapTeam2["Goals_SO"];
}

 QString Versus::qualifie(bool const rglAway) const {
    if (_diffs[0]> 0 || _diffs[2] > 0)
        return _teamsName[0];
    else if(_diffs[0] == 0 && rglAway && _diffs[1] > 0)
        return _teamsName[0];
    else if (_diffs[0] < 0 || _diffs[2] < 0)
        return _teamsName[1];
    else if(_diffs[0] == 0 && rglAway && _diffs[1] < 0)
        return _teamsName[1];
    else
        return "...";
}

