/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

/**     @class Représente un match
  *
  *     @todo ...
 **/

#ifndef MATCH_H
#define MATCH_H

#include "ui_Match.h"
#include "SLExpressions.h"

class SLTable;

class Match : public QFrame {
    Q_OBJECT

    public:
        //Methods
        Match(std::vector<QString> const &teamsName, std::vector< std::vector<int> > const &score, int const status, bool const noDraw, bool const inPenSh, SLTable *slTableLink = 0, QWidget *parent = 0);

        QString getXml() const;
        QString getHstName() const {return _teamsName[0];}
        QString getVstName() const {return _teamsName[1];}
        void setnvOrdre(int nvOrdre);
        void mayBeFinish(bool const isFinish);
        
        bool lessThan(Match const &b) const;

    signals:
        void contentsChanged();
        void matchMayBeFinish(Match *matchSender, QString hstName, QString vstName);
        void matchUpdated(QString hstName, QString vstName, std::map<QString,int> changeMapHst, std::map<QString,int> changeMapVst);
    
    protected:
        enum StatusFamily {
            Status_Aborted,
            Status_NotBegun,
            Status_Running,
            Status_Paused,
            Status_Finished
        };
        
        struct StatusItem {
            StatusItem(QString nwName, StatusFamily nwFamily, int nwInfosItem) {
                name = nwName;
                family = nwFamily;
                infosItem = nwInfosItem;
            }
            
            QString name;
            StatusFamily family;
            int infosItem;
        };
    
    private slots:
        void correction();
        void hstGoal();
        void vstGoal();
        void statusChanged();

    private:
        //Enums
        enum reason { Reason_HstGoal, Reason_VstGoal, Reason_StsRun2NoBg, Reason_StsNoBg2Run, Reason_StsRun2End, Reason_StsEnd2Run };
        //Methods
        void setBckGrdColor();
        void updateSLTable(int reason);
        void setScoreColor();
        
        static std::vector<StatusItem> setupStatusProgress() {
            std::vector<StatusItem> vect;
            
            //Match Aborted
            vect.push_back(StatusItem(tr("Annulé"), Status_Aborted, -1));
            vect.push_back(StatusItem(tr("Reporté"), Status_Aborted, -1));
            
            //Standard Match
            vect.push_back(StatusItem(tr("-----------", "Match non commencé"), Status_NotBegun, -1));
            vect.push_back(StatusItem(tr("1e Mi-Temps"), Status_Running, 0));
            vect.push_back(StatusItem(tr("Mi-Temps"), Status_Paused, 0));
            vect.push_back(StatusItem(tr("2e mi-Temps"), Status_Running, 0));
            
            //Extra-Time
            vect.push_back(StatusItem(tr("Fin Temps\nRéglementaire"), Status_Paused, 1));
            vect.push_back(StatusItem(tr("1e Mi-Temps\nProlongations"), Status_Running, 0));
            vect.push_back(StatusItem(tr("Mi-Temps\nProlongations"), Status_Paused, 0));
            vect.push_back(StatusItem(tr("2e Mi-Temps\nProlongations"), Status_Running, 0));
            
            //Penalty Shootout
            vect.push_back(StatusItem(tr("Fin\nProlongations"), Status_Paused, 2));
            vect.push_back(StatusItem(tr("Tirs\nAux Buts"), Status_Running, 0));
            
            //End of the Match
            vect.push_back(StatusItem(tr("Fin"), Status_Finished, 0));
            
            return vect;
        } 

        //Attributs
        Ui::Match ui;
        std::vector<QString> _teamsName;
        std::vector< std::vector<int> > _score;
        static std::vector<StatusItem> _StatusProgress;
        int _statusId;
        int _multiCorr; //Varie selon si "Correction" est activé (-1) ou non (1), pour les buts
        int _ordre; //Pour la re-definition de l'ordre avec "Journee - Modifier..."
        bool _extraTime; //Pour les prolongations (certains matchs de coupe)
        bool _inPenSh; //Pour les tirs au buts (certains matchs de coupe)
        bool _waitAns;
};


class sortMatch {
    public:
        bool operator() (Match const *a, Match const *b) {    //conteneur de pointeurs
            return a->lessThan(*b);
        }
};


#endif // MATCH_H
