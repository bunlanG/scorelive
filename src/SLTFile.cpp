/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive. If not, see <http://www.gnu.org/licenses/>.
*/

#include "SLTFile.h"
#include "SLWindow.h"

SLTFile::SLTFile(QWidget *parent) : QWidget(parent) {
}

void SLTFile::load(QString const &path, SLTournament *file) {
    if(file == 0)
        return;
    QDomDocument dom("SLTFile");
    QFile xml_doc(path);
    
    if(!xml_doc.open(QIODevice::ReadOnly)) {
            QMessageBox::warning(this, tr("Erreur à l'ouverture du document XML"), tr("Le document XML n'a pas pu être ouvert. Vérifiez que le nom est le bon et que le document est bien placÃ©"));
            return;
    }

    QString errorMsg;
    int errorLine;
    int errorColumn;
    if (!dom.setContent(&xml_doc, &errorMsg, &errorLine, &errorColumn)) {
         xml_doc.close();
         QMessageBox::warning(this, tr("Erreur à l'ouverture du document XML"), tr("Le document XML n'a pas pu être attribué à l'objet QDomDocument.\nMsg :%1\nLine :%2\nCol :%3")
                                                                                   .arg(errorMsg).arg(errorLine).arg(errorColumn));
         return;
    }

    file->hide();
    
    QDomElement dom_element = dom.documentElement();
    QDomNode node1 = dom_element.firstChild(); //SLTRoot

    /* SLT-parse version 1 (without Structure) */
    if (dom_element.attribute("fileVersion", "na") == "SLTv1") {
        while (!node1.isNull()) {
            QDomElement element1 = node1.toElement();
            
            parseSLTElements(element1, file);
            
            node1 = node1.nextSibling();
        }
    } //fileVersion == SLTv1

    /* test-file */
   if (dom_element.attribute("fileVersion", "na") == "test") {
      while (!node1.isNull()) {
         QDomElement element1 = node1.toElement();
         QString elmtTagName = element1.tagName();

         if(elmtTagName == "Data") {
            QDomNode nodeProp = element1.firstChild();

            while (!nodeProp.isNull()) {
               QDomElement elementProp = nodeProp.toElement();

               QString elmtTagName2 = elementProp.tagName();

               if(elmtTagName2 == "Prop") {
                  SLData *prop = SLData::instance();

                  QString propType = elementProp.attribute("t");
                  QString propId = elementProp.attribute("id");
                  QString propVal = elementProp.attribute("v");

                  prop->addProp(propType, propId, propVal);
               }

               nodeProp = nodeProp.nextSibling();
            }

         } else {
            parseSLTElements(element1, file);
         }

         node1 = node1.nextSibling();
      }
   }

   file->show();
}

void SLTFile::parseSLTElements(QDomElement const &elementSLT, SLTournament *file, std::vector<int> const &remark) {
    QString elmtTagName = elementSLT.tagName();
    
    if(elmtTagName == "Infos") {
        QString infosFileName = elementSLT.attribute("fileName");
        int infosOptHeight = elementSLT.attribute("optHeight").toInt();

        emit nvChp(infosFileName, infosOptHeight);
    }

    if(elmtTagName == "Content" || elmtTagName == "SLTables") {
        QDomNode nodeSLT = elementSLT.firstChild();

        while (!nodeSLT.isNull()) {
            QDomElement elementSLTChild = nodeSLT.toElement();

            parseSLTElements(elementSLTChild, file);

            nodeSLT = nodeSLT.nextSibling();
        }
    }

    if(elmtTagName == "RndRob") {
        int rndRobId = elementSLT.attribute("id").toInt();
        QString rndRobName = elementSLT.attribute("name");
        QString rndRobTitle = elementSLT.attribute("title");
        std::vector< std::vector<int> > rndRobBckGrdClr = SLData::expToIntMatr(elementSLT.attribute("bckGrdClr"));
        std::vector< int > rndRobCriterions = SLData::expToIntVect(elementSLT.attribute("criterions"));

        file->addItem(new SLTable(rndRobName, rndRobTitle, rndRobBckGrdClr, rndRobCriterions));

        QDomNode nodeRndRob = elementSLT.firstChild();

        while (!nodeRndRob.isNull()) {
            QDomElement elementRndRob = nodeRndRob.toElement();

            parseSLTElements(elementRndRob, file, std::vector<int>(1,rndRobId));

            nodeRndRob = nodeRndRob.nextSibling();
        }
    }

    if(elmtTagName == "Team") {
        int teamId = elementSLT.attribute("id").toInt();
        QString teamName = elementSLT.attribute("name");
        std::vector<int> teamWDLCorr = SLData::expToIntVect(elementSLT.attribute("WDLCorr"));
        std::vector<int> teamGls = SLData::expToIntVect(elementSLT.attribute("gls"));
        int teamMan = elementSLT.attribute("man").toInt();
        int teamPrevPos = elementSLT.attribute("prevPos").toInt();
        std::vector<int> teamPtsPart = SLData::expToIntVect(elementSLT.attribute("ptsPart"));
        std::vector<int> teamDiffPart = SLData::expToIntVect(elementSLT.attribute("diffPart"));
        std::vector<int> teamGFPart = SLData::expToIntVect(elementSLT.attribute("GFPart"));
        std::vector<int> teamAwayGFPart = SLData::expToIntVect(elementSLT.attribute("awayGFPart"));

        file->addItem(new Team(teamName, teamId, teamWDLCorr, teamGls, teamMan, teamPrevPos, teamPtsPart, teamDiffPart, teamGFPart, teamAwayGFPart), remark[0]);
    }

    if(elmtTagName == "KnkOut") {
        int knkOutId = elementSLT.attribute("id").toInt();
        QString knkOutName = elementSLT.attribute("name");
        QString knkOutTitle = elementSLT.attribute("title");
        bool knkOutRulAwayGls = true;
            if (elementSLT.attribute("rulAwayGls") == 0)
                knkOutRulAwayGls = false;

        file->addItem(new SLTable(knkOutName, knkOutTitle, knkOutRulAwayGls));

        QDomNode nodeKnkOut = elementSLT.firstChild();

        while (!nodeKnkOut.isNull()) {
            QDomElement elementKnkOut = nodeKnkOut.toElement();

            parseSLTElements(elementKnkOut, file, std::vector<int>(1,knkOutId));

            nodeKnkOut = nodeKnkOut.nextSibling();
        }
    }

    if(elmtTagName == "Versus") {
        int versusId = elementSLT.attribute("id").toInt();
        std::vector< QString > versusTeams = SLData::expToStrVect(elementSLT.attribute("teams"));
        std::vector< int > versusDiffs = SLData::expToIntVect(elementSLT.attribute("diffs"));

        file->addItem(new Versus(versusTeams, versusDiffs), remark[0]);
    }

    if(elmtTagName == "Results") {
        file->setFstJr(elementSLT.attribute("clsEna").toInt(), elementSLT.attribute("jrEna").toInt());

        QDomNode nodeResults = elementSLT.firstChild();

        while (!nodeResults.isNull()) {
            QDomElement elementResults = nodeResults.toElement();

            parseSLTElements(elementResults, file);

            nodeResults = nodeResults.nextSibling();
        }
    }

    if(elmtTagName == "MatchDay") {
        std::vector<int> matchDayId = SLData::expToIntVect(elementSLT.attribute("id"));
        QString matchDayTitle = elementSLT.attribute("title");
        SLTable *matchDaySLTablePtr = file->getSLTablePointor(matchDayId[0]); 

        file->addItem(new MatchDay(matchDayTitle, matchDaySLTablePtr), matchDayId[0]);

        QDomNode nodeMatchDay = elementSLT.firstChild();

        while (!nodeMatchDay.isNull()) {
            QDomElement elementMatchDay = nodeMatchDay.toElement();

            parseSLTElements(elementMatchDay, file, matchDayId);

            nodeMatchDay = nodeMatchDay.nextSibling();
        }
    }

    if(elmtTagName == "Match") {
        int matchId = elementSLT.attribute("id").toInt();
        std::vector< QString > matchTeams = SLData::expToStrVect(elementSLT.attribute("teams"));
        std::vector< std::vector<int> > matchScr = SLData::expToIntMatr(elementSLT.attribute("scr"));
        int matchSts = elementSLT.attribute("sts").toInt();
        bool matchNoDraw = true;
            if (elementSLT.attribute("noDraw").toInt() == 0)
                matchNoDraw = false;
        bool matchInPenSh = true;
            if (elementSLT.attribute("inPenSh").toInt() == 0)
                matchInPenSh = false;
        SLTable *matchSLTableLink = file->getSLTablePointor(remark[0]);

        file->addItem(new Match(matchTeams, matchScr, matchSts, matchNoDraw, matchInPenSh, matchSLTableLink), remark[0], remark[1]);
    }
}

void SLTFile::write(QString const &path, QString const &xmlString) {
    QFile file(path);
    
    if(!file.open(QIODevice::WriteOnly)) {
        file.close();
        QMessageBox::critical(this, tr("Erreur"), tr("Impossible d'écrire dans le document XML"));
        return;
    }
    
    QTextStream stream(&file);
    stream << xmlString;
    file.close();
}
