/*
Copyright (C) 2011-2014 GUILBAULT Ronan

This file is part of ScoreLive.

ScoreLive is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScoreLive is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ScoreLive.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Team.h"

Team::Team(const QString &name, const int id, const std::vector<int> &vectWDLCorr, const std::vector<int> &goals, const int man, const int prevPos, const std::vector<int> ptsPart, const std::vector<int> diffPart, const std::vector<int> vectGFPart, const std::vector<int> awayGFPart, QWidget *parent): QFrame(parent), _name(name), _id(id), _man(man), _ptsPartTt(0), _diffPartTt(0), _GFPartTt(0), _awayGFPartTt(0), _prevPos(prevPos), _man_ghst(0), _BMExt(0) {
    ui.setupUi(this);

    _ptsPart = ptsPart;
    _diffPart = diffPart;
    _GFPart = vectGFPart;
    _awayGFPart = awayGFPart;
        
    _WDLCorr = vectWDLCorr;
    _goals = goals;

    update();
}

QString Team::getXml() const {
    QString text;

    text = QString("\t\t\t\t<Team name=\"%1\" id=\"%2\" WDLCorr=\"%3\" gls=\"%4\" man=\"%5\" prevPos=\"%6\" %7%8%9%10/>\n")
           .arg(_name)
           .arg(_id)
           .arg(SLExpressions::intVectToExp(_WDLCorr))
           .arg(SLExpressions::intVectToExp(_goals))
           .arg(_man)
           .arg(_prevPos)
           .arg(SLExpressions::intVectToExp(_ptsPart, "ptsPart"))
           .arg(SLExpressions::intVectToExp(_diffPart, "diffPart"))
           .arg(SLExpressions::intVectToExp(_GFPart, "GFPart"))
           .arg(SLExpressions::intVectToExp(_awayGFPart, "awayGFPart"));

    return text;
}

void Team::updateView(QString const &newPosStr, QString const &newDiffPosStr, QString const &newStyleSheetStr) {
    ui.pos->setText(newPosStr);
    
    ui.name->setText(getName());
    ui.points->setText(QString::number(_pts));
    ui.mPlayed->setText(QString::number(_MPlyd));
    ui.mWins->setText(QString::number(_WDLCorr[0]));
    ui.mDraws->setText(QString::number(_WDLCorr[1]));
    ui.mLoses->setText(QString::number(_WDLCorr[2]));
    ui.gFor->setText(QString::number(_goals[0]));
    ui.gAgnst->setText(QString::number(_goals[1]));
    ui.gDiff->setText(QString::number(_diff));
    
    ui.diffPos->setText(newDiffPosStr);
    ui.diffPos->setStyleSheet(newStyleSheetStr);     
}

void Team::update(int const idAdv, bool const isHost, std::map<QString,int> changeMap, std::map<QString,int> changeMapAdv) {
    int chgWins = 0;
    int chgDrws = 0;
    int chgLoss = 0;
    int chgGlFr = 0;
    int chgGlAg = 0;
    
    if(changeMap.find("Wins") != changeMap.end())
        chgWins = changeMap["Wins"];
    if(changeMap.find("Draws") != changeMap.end())
        chgDrws = changeMap["Draws"];
    if(changeMap.find("Loses") != changeMap.end())
        chgLoss = changeMap["Loses"];
    if(changeMap.find("Goals") != changeMap.end())
        chgGlFr = changeMap["Goals"];
    if(changeMapAdv.find("Goals") != changeMapAdv.end())
        chgGlAg = changeMapAdv["Goals"]; 
    
    _WDLCorr[0] += chgWins;
    _WDLCorr[1] += chgDrws;
    _WDLCorr[2] += chgLoss;
    _goals[0] += chgGlFr;
    _goals[1] += chgGlAg;

    _pts = 3 * _WDLCorr[0] + _WDLCorr[1] + _WDLCorr[3];
    _MPlyd = _WDLCorr[0] + _WDLCorr[1] + _WDLCorr[2];
    _diff = _goals[0] - _goals[1];

    if(idAdv >= 0 && idAdv < _diffPart.size()) //Cls Standard et Special
        _diffPart[idAdv] += chgGlFr - chgGlAg;

    if(idAdv >= 0 && idAdv < _ptsPart.size()) //Cls Special UNIQUEMENT !
        _ptsPart[idAdv] += 3 * chgWins + chgDrws;
    if(idAdv >= 0 && idAdv < _GFPart.size())
        _GFPart[idAdv] += chgGlFr;
    if(idAdv >= 0 && idAdv < _awayGFPart.size() && !isHost)
        _awayGFPart[idAdv] += chgGlFr;
}

void Team::maj_sort(std::vector<int> const &idTab) {
    _ptsPartTt = 0; _diffPartTt = 0; _GFPartTt = 0; _awayGFPartTt = 0;

    for(int i = 0 ; i < idTab.size() ; i++) {
        _ptsPartTt += getPtsPart(idTab[i]);
        _diffPartTt += getDiffPart(idTab[i]);
        _GFPartTt += getBMPart(idTab[i]);
        _awayGFPartTt += getBMExtPart(idTab[i]);
    }
}

/// @part criteres de tris
bool Team::pts_g(Team const *a, Team const *b){
    if(a->_pts > b->_pts)
        return true;

    else if(a->_pts == b->_pts) {
        return a->_name < b->_name;
    }

    return false;
}

bool Team::spe_acBMExt(Team const *a, Team const *b) {
    if(a->_ptsPartTt > b->_ptsPartTt)
        return true;

    else if(a->_ptsPartTt == b->_ptsPartTt) {
        if(a->_diffPartTt > b->_diffPartTt)
            return true;

        else if(a->_diffPartTt == b->_diffPartTt) {
            if(a->_GFPartTt > b->_GFPartTt)
                return true;

            else if(a->_GFPartTt == b->_GFPartTt) {
                if(a->_awayGFPartTt > b->_awayGFPartTt)
                    return true;

                else if(a->_awayGFPartTt == b->_awayGFPartTt) {
                    return a->_name < b->_name;
                }
            }
        }
    }

    return false;
}

bool Team::spe_ssBMExt(Team const *a, Team const *b) {
    if(a->_ptsPartTt > b->_ptsPartTt)
        return true;

    else if(a->_ptsPartTt == b->_ptsPartTt) {
        if(a->_diffPartTt > b->_diffPartTt)
            return true;

        else if(a->_diffPartTt == b->_diffPartTt) {
            if(a->_GFPartTt > b->_GFPartTt)
                return true;

            else if(a->_GFPartTt == b->_GFPartTt) {
                return a->_name < b->_name;
            }
        }
    }

    return false;
}

bool Team::stdd(Team const *a, Team const *b) {
    if(a->_diff > b->_diff)
        return true;

    else if(a->_diff == b->_diff) {
        if(a->_goals[0] > b->_goals[0])
            return true;

        else if(a->_goals[0] == b->_goals[0]) {
            return a->_name < b->_name;
        }
    }

    return false;
}

bool Team::diff_p(Team const *a, Team const *b) {
    if(a->_diffPartTt > b->_diffPartTt)
        return true;

    else if(a->_diffPartTt == b->_diffPartTt) {
        return a->_name < b->_name;
    }

    return false;
}

bool Team::man(Team const *a, Team const *b) {
    if(a->_man < b->_man)
        return true;

    else if(a->_man == b->_man) {
        return a->_name < b->_name;
    }

    return false;
}

bool Team::findByName(QString const &nameSeeked) {
    return (_name == nameSeeked);
}

bool Team::estEgalA(Team const *b, int const crit) {
    switch(crit) {
        case CRIT_PTS_G:
            return (_pts == b->_pts);
        break;
        case CRIT_SPE_SSBMEXT: case CRIT_SPE_SSBMEXT_REC:
            return (_ptsPartTt == b->_ptsPartTt && _diffPartTt == b->_diffPartTt && _GFPartTt == b->_GFPartTt && _awayGFPartTt == b->_awayGFPartTt);
        break;
        case CRIT_SPE_ACBMEXT: case CRIT_SPE_ACBMEXT_REC:
            return (_ptsPartTt == b->_ptsPartTt && _diffPartTt == b->_diffPartTt && _GFPartTt == b->_GFPartTt);
        break;
        case CRIT_STD:
            return (_diff == b->_diff && _goals[0] == b->_goals[0]);
        break;
        case CRIT_DIFF_P:
            return (_diffPartTt == b->_diffPartTt);
        break;
        case CRIT_MAN:
            return (_man == b->_man);
        break;
        case CRIT_ALL:
            return (_pts == b->_pts && _ptsPartTt == b->_ptsPartTt && _diffPartTt == b->_diffPartTt && _GFPartTt == b->_GFPartTt && _awayGFPartTt == b->_awayGFPartTt && _diff == b->_diff && _goals[0] == b->_goals[0]);
        break;
        default:
            return false;
        break;
    }
}
/// @endpart criteres de tris

int Team::getPtsPart(int const id) const {
    if(id >= 0 && id < _ptsPart.size())
        return _ptsPart[id];
    else
        return 0;
}

int Team::getDiffPart(int const id) const {
    if(id >= 0 && id < _diffPart.size())
        return _diffPart[id];
    else
        return 0;
}

int Team::getBMPart(int const id) const {
    if(id >= 0 && id < _GFPart.size())
        return _GFPart[id];
    else
        return 0;
}

int Team::getBMExtPart(int const id) const {
    if(id >= 0 && id < _awayGFPart.size())
        return _awayGFPart[id];
    else
        return 0;
}

void Team::resetPrevPos(int const nvPrevPos) {
    _prevPos = nvPrevPos;
    
    ui.diffPos->setText(QString::fromUtf8("\u25C4")+QString::fromUtf8("\u25BA"));
    ui.diffPos->setStyleSheet(QString::fromUtf8("color: rgb(64, 64, 64);\n"
                                                "background-color: rgb(0, 0, 0);"));
}
